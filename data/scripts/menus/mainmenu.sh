#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Main menu

mainmenu() {
  header
  menutitle "Main Menu"
  echo "${GREEN}1)${NORMAL} Set ROM Version (Current: ${GREEN}${buildversion}${NORMAL})"
  echo "${GREEN}2)${NORMAL} Download ROMs"
  echo "${GREEN}3)${NORMAL} Deodex ROMs"
  echo "${GREEN}4)${NORMAL} Translation tools"
  echo "${GREEN}5)${NORMAL} Build ROMs"
  echo ""
  echo "${RED}X)${NORMAL} Exit system"
  footer
  echo -n "Select option: "
  read -n1 -r -s choice
  case ${choice} in
  1)
    set_romversion
    unset choice
    mainmenu
    ;;
  2)
    download_roms
    unset choice
    mainmenu
    ;;
  3)
    deodex_roms
    unset choice
    mainmenu
    ;;
  4)
    translation_tools
    unset choice
    mainmenu
    ;;
  5)
    build_roms
    unset choice
    mainmenu
    ;;
  x | X)
    unset choice
    clear
    exit
    ;;
  *)
    echo "${BOLD}Unknown option${NORMAL}"
    sleep 0.5
    mainmenu
    ;;
  esac
}
