#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Menu item - Deodex ROMs

deodex_roms() {
  header
  menutitle "Main Menu" "Deodex ROMs"
  echo -e "Currently deodexed ROMs: ${BLUE}$(ls -1 ${mrc_deodexed_rom}/*.zip 2>/dev/null | wc -l)${NORMAL}\n"
  echo "${GREEN}1)${NORMAL} Deodex a specific ROM"
  echo "${GREEN}2)${NORMAL} Deodex a specific Android version"
  echo "${GREEN}3)${NORMAL} Deodex all ROMs"
  echo ""
  echo "${RED}B)${NORMAL} Go Back"
  footer
  echo -n "Select option: "
  read -n1 -r -s submenu
  case ${submenu} in
  1)
    deodex_specific
    unset submenu
    deodex_roms
    ;;
  2)
    deodex_android
    unset submenu
    deodex_roms
    ;;
  3)
    deodex_all
    unset submenu
    deodex_roms
    ;;
  b | B)
    unset submenu
    mainmenu
    ;;
  *)
    echo "${BOLD}Unknown option${NORMAL}"
    sleep 0.5
    deodex_roms
    ;;
  esac
}

deodex_specific() {
  unset fid
  unset number
  unset fnames
  header
  menutitle "Main Menu" "Deodex ROMs" "Deodex a specific ROM"

  cleanup

  number=1

  for file in ${mrc_source_rom}/*.zip; do
    fnames+=($(basename -s .zip ${file}))
    echo "${GREEN}${number})${NORMAL}" $(basename -s .zip "${file}")
    let "number += 1"
  done

  footer
  read -r -p "Select a file: " fid
  fid=$((${fid} - 1))
  cd ${mrc_source_rom} || exit
  export sourcezip="${fnames[${fid}]}.zip"
  if [[ ${sourcezip} == "" ]]; then
    echo -e "${RED}ERROR:${NORMAL}\n"
    echo "No ROM was found for the number inserted."
    echo "Make sure that you're typing it correctly, and try again."
    footer
    presskey
  else
    header
    menutitle "Main Menu" "Deodex ROMs" "${sourcezip}"

    export logname=$(echo "${sourcezip}" | cut -d "_" -f2)
    export rombase=$(echo "${sourcezip}" | cut -d "_" -f5 | cut -d "." -f1-2)
    if [[ ${buildversion} == "Auto" ]]; then
      export versioninfo=$(echo "${sourcezip}" | cut -d "_" -f3)
    else
      export versioninfo=${buildversion}
    fi

    if [[ -f ${mrc_log_dir}/MRC-Deodex-${logname}.log ]]; then
      rm -rf ${mrc_log_dir}/MRC-Deodex-${logname}.log
    fi

    RUN_LOG=${mrc_log_dir}/MRC-Deodex-${logname}.log

    extract_romzip

    pre_deodex_plugins

    if [[ ${rombase} == "5.0" || ${rombase} == "5.1" || ${rombase} == "6.0" ]]; then
      run_ok "${deomm} ${mrc_work_dir}/system" "Deodexing ROM"
    elif [[ ${rombase} == "7.0" || ${rombase} == "7.1" ]]; then
      run_ok "deodex_start_no" "Deodexing ROM"
    elif [[ ${rombase} == "8.0" || ${rombase} == "8.1" || ${rombase} == "9.0" ]]; then
      run_ok "deodex_start_p" "Deodexing ROM"
    else
      echo "ROM API incompatible with the compiler"
      sleep 2
      return
    fi

    run_ok "zipalign_apps" "ZipAligning deodexed apps"

    post_deodex_plugins

    run_ok "patch_kernel" "Patching kernel"

    run_ok "finish_deodex" "Compressing deodexed ROM"

    echo -e "\n${sourcezip} deodex finished."
    footer
    presskey

    cleanup

  fi
  RUN_LOG=${mrc_log_dir}/MRC.log
}

deodex_android() {
  header
  menutitle "Main Menu" "Deodex ROMs" "Deodex a specific Android version"
  echo "${GREEN}1${NORMAL}) Android 6.X (Marshmallow)"
  echo "${GREEN}2${NORMAL}) Android 7.X (Nougat)"
  echo "${GREEN}3${NORMAL}) Android 8.X (Oreo)"
  echo "${GREEN}4${NORMAL}) Android 9.X (Pie)"
  echo -e "\n${RED}B${NORMAL}) Go back"
  footer
  echo -n "Select option: "
  read -n1 -r -s deochoice
  case ${deochoice} in
  1) export selectedav="6" ;;
  2) export selectedav="7" ;;
  3) export selectedav="8" ;;
  4) export selectedav="9" ;;
  b | B) deodex_roms ;;
  *)
    echo "${BOLD}Unknown Option${NORMAL}"
    sleep 0.5
    deodex_android
    ;;
  esac
  cleanup
  cd ${mrc_source_rom} || exit
  for sourcezip in $(ls *${selectedav}.0.zip *${selectedav}.1.zip 2>/dev/null); do
    cleanup
    header
    menutitle "Main Menu" "Deodex ROMs" "${sourcezip}"
    export logname=$(echo "${sourcezip}" | cut -d "_" -f2)
    export rombase=$(echo "${sourcezip}" | cut -d "_" -f5 | cut -d "." -f1-2)
    if [[ ${buildversion} == "Auto" ]]; then
      export versioninfo=$(echo "${sourcezip}" | cut -d "_" -f3)
    else
      export versioninfo=${buildversion}
    fi

    if [[ -f ${mrc_log_dir}/MRC-Deodex-${logname}.log ]]; then
      rm -rf ${mrc_log_dir}/MRC-Deodex-${logname}.log
    fi

    RUN_LOG=${mrc_log_dir}/MRC-Deodex-${logname}.log

    extract_romzip

    pre_deodex_plugins

    if [[ ${rombase} == "5.0" || ${rombase} == "5.1" || ${rombase} == "6.0" ]]; then
      run_ok "${deomm} ${mrc_work_dir}/system" "Deodexing ROM"
    elif [[ ${rombase} == "7.0" || ${rombase} == "7.1" ]]; then
      run_ok "deodex_start_no" "Deodexing ROM"
    elif [[ ${rombase} == "8.0" || ${rombase} == "8.1" || ${rombase} == "9.0" ]]; then
      run_ok "deodex_start_p" "Deodexing ROM"
    else
      echo "ROM API incompatible with the compiler"
      sleep 2
      return
    fi

    run_ok "zipalign_apps" "ZipAligning deodexed apps"

    post_deodex_plugins

    run_ok "patch_kernel" "Patching kernel"

    run_ok "finish_deodex" "Compressing deodexed ROM"

    echo -e "\n${sourcezip} deodex finished."
    footer
  done
  header
  menutitle "Main Menu" "Deodex ROMs" "Deodex all ROMs"
  echo -e "All ROMs were deodexed."
  echo "Logfiles for deodexing process can be found at ${mrc_log_dir}"
  footer
  presskey
  cleanup
  RUN_LOG=${mrc_log_dir}/MRC.log
}

deodex_all() {
  cd ${mrc_source_rom} || exit
  for sourcezip in *.zip; do
    cleanup
    header
    menutitle "Main Menu" "Deodex ROMs" "${sourcezip}"
    export logname=$(echo "${sourcezip}" | cut -d "_" -f2)
    export rombase=$(echo "${sourcezip}" | cut -d "_" -f5 | cut -d "." -f1-2)
    if [[ ${buildversion} == "Auto" ]]; then
      export versioninfo=$(echo "${sourcezip}" | cut -d "_" -f3)
    else
      export versioninfo=${buildversion}
    fi

    if [[ -f ${mrc_log_dir}/MRC-Deodex-${logname}.log ]]; then
      rm -rf ${mrc_log_dir}/MRC-Deodex-${logname}.log
    fi

    RUN_LOG=${mrc_log_dir}/MRC-Deodex-${logname}.log

    extract_romzip

    pre_deodex_plugins

    if [[ ${rombase} == "5.0" || ${rombase} == "5.1" || ${rombase} == "6.0" ]]; then
      run_ok "${deomm} ${mrc_work_dir}/system" "Deodexing ROM"
    elif [[ ${rombase} == "7.0" || ${rombase} == "7.1" ]]; then
      run_ok "deodex_start_no" "Deodexing ROM"
    elif [[ ${rombase} == "8.0" || ${rombase} == "8.1" || ${rombase} == "9.0" ]]; then
      run_ok "deodex_start_p" "Deodexing ROM"
    else
      echo "ROM API incompatible with the compiler"
      sleep 2
      return
    fi

    run_ok "zipalign_apps" "ZipAligning deodexed apps"

    post_deodex_plugins

    run_ok "patch_kernel" "Patching kernel"

    run_ok "finish_deodex" "Compressing deodexed ROM"

    echo -e "\n${sourcezip} deodex finished."
    footer
  done
  header
  menutitle "Main Menu" "Deodex ROMs" "Deodex all ROMs"
  echo -e "All ROMs were deodexed."
  echo "Logfiles for deodexing process can be found at ${mrc_log_dir}"
  footer
  presskey
  cleanup
  RUN_LOG=${mrc_log_dir}/MRC.log
}
