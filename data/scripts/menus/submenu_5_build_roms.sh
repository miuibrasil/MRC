#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Build ROMs

build_roms() {
  header
  menutitle "Main Menu" "Build ROMs"
  echo "${GREEN}1)${NORMAL} Build a single ROM"
  echo "${GREEN}2)${NORMAL} Build China ROMs"
  echo "${GREEN}3)${NORMAL} Build Global ROMs"
  echo "${GREEN}4)${NORMAL} Build all ROMs"
  echo ""
  echo "${RED}B)${NORMAL} Go Back"
  echo ""
  separator
  echo "Auto Upload ROMs: $(autouploader). To change this, press ${BOLD}A${NORMAL}."
  separator
  echo -n "Select option: "
  read -n1 -r -s choice
  case ${choice} in
  1)
    build_single
    unset choice
    build_roms
    ;;
  2)
    build_china
    unset choice
    build_roms
    ;;
  3)
    build_global
    unset choice
    build_roms
    ;;
  4)
    build_all
    unset choice
    build_roms
    ;;
  a | A)
    autoupload_toggle
    unset choice
    build_roms
    ;;
  b | B)
    unset choice
    mainmenu
    ;;
  *)
    echo "${BOLD}Unknown option${NORMAL}"
    sleep 0.5
    unset choice
    build_roms
    ;;
  esac
}
