#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Menu item - Download ROMs

download_roms() {
  header
  menutitle "Main Menu" "Download ROMs"
  echo -e "Instructions:\n"
  echo "Prepare a list with all ROM URLs that are going do be downloaded."
  echo "On the next screen, paste all URLs on the opened text editor."
  echo "${BOLD}Make sure that you paste only one URL per line.${NORMAL}"
  echo "To save the file after pasting all URLs, press ${BOLD}CTRL+O${NORMAL}."
  echo "After saving, press ${BOLD}CTRL+X${NORMAL} to exit the text editor."
  echo "The download process will start immediately after that."
  footer
  presskey
  header
  menutitle "Main Menu" "Download ROMs"
  cd ${mrc_source_rom} || exit # Moves to the ROM input folder to prepare for the download
  nano romlist                 # Opens nano text editor to paste ROM URls.
  if [[ ! -f romlist ]]; then # If rom.list file doesnt exists...
    echo -e "${BOLD}${RED}ERROR:${NORMAL}\n" # Throw error informing user to try again.
    echo "It appears that you haven't added any URLs to be downloaded."
    echo "Please, try again following the instructions on the Download ROMs screen."
    footer
    presskey
  else # If romlist file exists...
    for mrc_rom_url in $(<romlist); do
      run_ok "axel -a -n5 -q ${mrc_rom_url}" "Downloading $(echo ${mrc_rom_url} | cut -d'/' -f5)" # ...downloads it, storing the resulting file on the ROM input folder.
    done
    echo -e "\nAll ROM files were downloaded."
    rm -rf romlist # After all ROMs are downloaded, delete used romlist file.
    footer
    presskey
  fi
  cd ${mrc_system} || exit # Moves back to the compiler main directory to avoid any error on next functions.
}
