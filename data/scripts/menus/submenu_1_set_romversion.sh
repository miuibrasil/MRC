#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Menu item - Set ROM Version

set_romversion() {
  header
  menutitle "Main Menu" "Set ROM Version"
  echo "Insert desired version (${BOLD}X.X.X${NORMAL})"
  echo "Press ${BOLD}Return${NORMAL} to reset ROM version to Automatic Mode."
  echo "In Automatic Mode, ROMs will use it's default version contained on the filename."
  footer
  echo -n "Enter desired version: "
  read -r buildversion
  if [[ ${buildversion} == "" ]]; then
    export buildversion="Auto"
  else
    export buildversion
  fi
}
