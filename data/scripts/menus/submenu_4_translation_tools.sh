#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Translation tools menu

translation_tools() {
  header
  menutitle "Main Menu" "Translation tools"
  echo "${GREEN}1)${NORMAL} Extract English XML files"
  echo "${GREEN}2)${NORMAL} Extract Global XML files"
  echo "${GREEN}3)${NORMAL} Upload English XMLs to Crowdin"
  echo "${GREEN}4)${NORMAL} Upload Global XMLs to Crowdin"
  echo "${GREEN}5)${NORMAL} Update local repositories"
  echo "${GREEN}6)${NORMAL} Create app pack for patch testing"
  echo "${GREEN}7)${NORMAL} Test patches on apk packs"
  echo ""
  echo "${RED}B)${NORMAL} Go Back"
  footer
  echo -n "Select option: "
  read -n1 -r -s choice
  case ${choice} in
  1)
    extract_english_xml
    unset choice
    translation_tools
    ;;
  2)
    extract_global_xml
    unset choice
    translation_tools
    ;;
  3)
    upload_source_to_crowdin
    unset choice
    translation_tools
    ;;
  4)
    upload_global_to_crowdin
    unset choice
    translation_tools
    ;;
  5)
    update_local_repositories
    unset choice
    translation_tools
    ;;
  6)
    create_app_packs
    unset choice
    translation_tools
    ;;
  7)
    test_patches
    unset choice
    translation_tools
    ;;
  b | B)
    unset choice
    mainmenu
    ;;
  *)
    echo "${BOLD}Unknown option${NORMAL}"
    sleep 0.5
    translation_tools
    ;;
  esac
}
