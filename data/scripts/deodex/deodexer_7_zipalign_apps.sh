#!/bin/bash
# MRC - MIUI ROM Compiler
# SuperR work for MIUI Brasil
# Deodex script - Zip Aligner

zipalign_apps() {
  cd ${mrc_work_dir} || exit
  find system vendor -name *.apk 2>/dev/null | grep -v "/framework/" | sed 's/^\.\///' | sort | while read line; do
    app=$(basename ${line})
    ${zipalign} -f 4 ${line} ${line}-2 >/dev/null 2>&1
    if [[ -f ${line}-2 ]]; then
      mv ${line}-2 ${line}
    fi
  done
  find system/framework -type f | grep .apk | grep -v "${mrc_work_dir_project}" | sed 's/^\.\///' | while read line; do
    app=$(basename ${line})
    ${zipalign} -f 4 ${line} ${line}-2 >/dev/null 2>&1
    if [[ -f ${line}-2 ]]; then
      mv ${line}-2 ${line}
    fi
  done
}
