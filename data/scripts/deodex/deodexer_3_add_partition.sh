#!/bin/bash
# MRC - MIUI ROM Compiler
# SuperR work for MIUI Brasil
# Deodex script - Add partitions to updater-script

add_partition() {
  cd ${mrc_work_dir_updater} || exit
  gawk 'a=/^mount/{b=1}b&&!a{print "#MOUNT";b=0}1' updater-script >updater-script2
  mv updater-script2 updater-script
  sed -i "s/#MOUNT/mount(\"ext4\"\,\ \"EMMC\"\,\ file_getprop\(\"\/tmp\/config\",\ \"${whatimg}\"\),\ \"\/${whatimg}\");/" updater-script
  gawk 'a=/^ifelse\(is_mounted/{b=1}b&&!a{print "#UNMOUNT1";b=0}1' updater-script >updater-script2
  mv updater-script2 updater-script
  gawk 'a=/^unmount/{b=1}b&&!a{print "#UNMOUNT2";b=0}1' updater-script >updater-script2
  mv updater-script2 updater-script
  sed -i "s/#UNMOUNT1/ifelse(is_mounted(\"\/${whatimg}\"),\ unmount(\"\/${whatimg}\"));/" updater-script
  sed -i "s/#UNMOUNT2/unmount(\"\/${whatimg}\");/" updater-script
  if [[ ! ${whatimg} == "data" ]]; then
    gawk 'a=/^format/{b=1}b&&!a{print "#FORMAT";b=0}1' updater-script >updater-script2
    mv updater-script2 updater-script
    sed -i "s/#FORMAT/format(\"ext4\"\,\ \"EMMC\"\,\ file_getprop\(\"\/tmp\/config\",\ \"${whatimg}\"\),\ \"0\"\,\ \"\/${whatimg}\");/" updater-script
  fi
  if [[ -s ${mrc_work_dir_project}/exdirs && $(grep "${whatimg}" ${mrc_work_dir_project}/exdirs) ]]; then
    partup=$(echo ${whatimg} | gawk '{print toupper($0)}')
    if [[ ! $(grep "#${partup}" updater-script) ]]; then
      line=$(grep "package_extract_dir(\"system\"" updater-script | sed 's/\"/\\"/g; s:\/:\\/:g')
      sed -i "s:${line}:${line}\n#${partup}:" updater-script
    fi
    if [[ ${whatimg} == "vendor" ]]; then
      sed -i "/#${partup}/ r ${mrc_scripts_deodex_dir}/convert_tools/vendor-set_metadata.txt" updater-script
    else
      sed -i "/#${partup}/ r ${mrc_scripts_deodex_dir}/convert_tools/extra-set_metadata.txt" updater-script
    fi
    sed -i "s/#PEXTRA/${whatimg}/g" updater-script
    touch ${mrc_work_dir_project}/${whatimg}img
    if [[ ${whatimg} == "vendor" && -f ${mrc_work_dir_project}/set_metadataV ]]; then
      cat ${mrc_work_dir_project}/set_metadataV >>${mrc_work_dir_project}/set_metadata
    fi
  elif [[ ${whatimg} == "data" ]]; then
    gawk 'a=/\"system\",/{b=1}b&&!a{print "#DATA";b=0}1' updater-script >updater-script2
    mv updater-script2 updater-script
    sed -i "/#DATA/ r ${mrc_scripts_deodex_dir}/convert_tools/data-set_metadata.txt" updater-script
    touch ${mrc_work_dir_project}/data-set_metadata
  fi
  whatimg=""
  cd ${mrc_work_dir} || exit
}
