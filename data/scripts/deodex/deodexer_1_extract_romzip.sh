#!/bin/bash
# MRC - MIUI ROM Compiler
# SuperR work for MIUI Brasil
# Deodex script - Extract ROM Zip

extract_romzip() {
  cd ${mrc_source_rom} || exit
  if [[ $(${p7z} l ${sourcezip} | grep system.new.dat) ]]; then
    run_ok "${p7z} x ${sourcezip} firmware-update compatibility.zip *.new.dat* *.transfer.list *.img *.bin -o${mrc_work_dir} 2>/dev/null" "Extracting required images and firmwares"
    cd ${mrc_work_dir} || exit
    ls | grep "\.new\.dat" | while read i; do
      line=$(echo "${i}" | cut -d "." -f1)
      if [[ $(echo "${i}" | grep "\.dat\.xz") ]]; then
        run_ok "${p7z} e ${i} 2>/dev/null" "Unpacking ${i}"
        rm -f ${i}
      fi
      if [[ $(echo "${i}" | grep "\.dat\.br") ]]; then
        run_ok "${brotli} -d ${i}" "Unpacking ${i}"
        rm -f ${i}
      fi
      run_ok "${sdat2img} ${line}.transfer.list ${line}.new.dat ${line}.img" "Converting ${line} to EXT4 image"
      rm -rf ${line}.transfer.list ${line}.new.dat
    done
    romimg="system.img"
  elif [[ $(${p7z} l ${sourcezip} | grep system.img) ]]; then
    run_ok "${p7z} x ${sourcezip} firmware-update *.img *.bin -o${mrc_work_dir} 2>/dev/null" "Extracting required EXT4 images and firmware files"
    romimg="system.img"
  else
    header
    menutitle "Main Menu" "Deodex ROMs"
    echo "${RED}This ROM format isn't compatible with our Compiler.${NORMAL}"
    echo "${RED}Our Compiler only works with .dat or .img ROMs.${NORMAL}"
    read -n1 -r -p "Press any key to continue..."
    cd ${mrc_system} || exit
    mainmenu
  fi
  romimgdir="$(echo ${romimg} | rev | cut -d '.' -f2- | rev)"
  extractimg="${romimgdir}"
  run_ok "img_extract" "Extracting ${extractimg} contents, contexts and metadatas"
  if [[ -f ${mrc_work_dir}/system/init.rc && -d ${mrc_work_dir}/system/system/app ]]; then
    sysdir="${mrc_work_dir}/system/system"
  else
    sysdir="${mrc_work_dir}/system"
  fi
  if [[ -f ${mrc_work_dir}/${romimgdir}/build.prop && ! ${romimgdir} == "system" && ! ${romimg} == "vendor.img" ]]; then
    mv ${mrc_work_dir}/${romimgdir} ${sysdir}
  fi
  framedir=${sysdir}/framework
  appdir=${sysdir}/app
  privdir=${sysdir}/priv-app
  if [[ -f ${sysdir}/build.prop ]]; then
    if [[ $(ls | grep "\.img$" | grep -v "system\|cache\|boot\|recovery\|preloader\|firstboot\|secondboot\|trustzone") ]]; then
      for line in $(ls | grep "\.img$" | grep -v "system\|cache\|boot\|recovery\|preloader\|firstboot\|secondboot\|trustzone"); do
        line2=$(echo "${line}" | sed 's/\.img//')
        extractimg="${line2}"
        run_ok "img_extract" "Extracting ${extractimg} contents, contexts and metadatas"
      done
    fi
    androidversion=$(grep "ro.build.version.release" ${sysdir}/build.prop | cut -d "=" -f2)
    api=$(grep "ro.build.version.sdk" ${sysdir}/build.prop | cut -d "=" -f2)
    get_devicename
    cp -R ${mrc_scripts_deodex_dir}/convert_tools/META-INF/ ${mrc_work_dir}/
    cp -R ${mrc_scripts_deodex_dir}/convert_tools/install ${mrc_work_dir}/
    link1=$(find system vendor -type l -printf "%l\n" 2>/dev/null | grep -v ".vdex" | sed 's/^/symlink(\"/; s/$/\", /')
    link2=$(find system vendor -type l 2>/dev/null | grep -v ".vdex" | sed 's/^/\"\//; s/$/\");/')
    paste -d '' <(echo "${link1}") <(echo "${link2}") | sort >${mrc_work_dir_project}/symlinks
    if [[ $(grep "symlink(\"egl/" ${mrc_work_dir_project}/symlinks) ]]; then
      sed -i 's/symlink(\"egl/symlink(\"\/egl/' ${mrc_work_dir_project}/symlinks
    fi
    cp ${mrc_work_dir_project}/symlinks ${mrc_work_dir_project}/symlinks.orig
    find system -type l -exec rm -f {} \;
    run_ok "update_project" "Creating updater-script structure"
  else
    echo "Failed to extract ${sourcezip}."
    footer
    read -n1 -r -p "Press any key to continue..."
    mainmenu
  fi
}
