#!/bin/bash
# MRC - MIUI ROM Compiler
# SuperR work for MIUI Brasil
# Deodex script - Update project files

update_project() {
  cd ${mrc_work_dir} || exit
  permtype="set_metadata"
  romdir2=$(echo "${mrc_work_dir}" | sed 's:\/:\\/:g')
  find ${sysdir} ${mrc_work_dir}/vendor 2>/dev/null | sed "s/${romdir2}//g" | sort >${mrc_work_dir_project}/all_files.txt
  cd ${mrc_work_dir_project} || exit
  rm -rf set_metadata
  cp set_metadata1 set_metadata
  if [[ -s root_meta ]]; then
    cat root_meta >>set_metadata
  fi
  rm -rf symlinks
  cp symlinks.orig symlinks
  cp ${mrc_scripts_deodex_dir}/convert_tools/custom/radio/${devicename} ${mrc_work_dir_project}/radioscript
  grep "/system/app\|/system/priv-app" all_files.txt | cut -d"/" -f1-4 | grep -v "^/system/app$\|^/system/priv-app$" | sort -u >>appsym
  line=""
  grep "/system/app" symlinks | cut -d"\"" -f4 | cut -d"/" -f1-4 | while read line; do
    if [[ ! "${line}" == $(grep "^${line}$" appsym) ]]; then
      grep -v "${line}" symlinks >symlinks2
      mv symlinks2 symlinks
    fi
  done
  line=""
  grep "/system/priv-app" symlinks | cut -d"\"" -f4 | cut -d"/" -f1-4 | while read line; do
    if [[ ! "${line}" == $(grep "^${line}$" appsym) ]]; then
      grep -v "${line}" symlinks >symlinks2
      mv symlinks2 symlinks
    fi
  done
  sort -u symlinks >symlinks2
  mv symlinks2 symlinks
  rm -rf appsym
  devname1=$(echo "${devicename}" | sed 's/\ /\\ /g')
  if [[ ! -f assert ]]; then
    cat ${mrc_scripts_deodex_dir}/convert_tools/custom/assert >>assert
    cat ${mrc_scripts_deodex_dir}/convert_tools/custom/abort >>assert
    sed -i "s/#DEVICENAME/${devname1}/g" assert
    sed -i "s/#DEVICECHK/${devicechk}/g" assert
    if [[ -f assertcustom ]]; then
      grep "ro.product.device" assert >assert-2
      mv assert-2 assert
      cat assertcustom >>assert
    fi
  fi
  if [[ ! $(grep " getprop(\|(getprop(" ${mrc_work_dir_updater}/updater-script) ]]; then
    sed -i '/#ASSERT/ r assert' ${mrc_work_dir_updater}/updater-script
  fi
  sed -i "s/#DEVICENAME/${devname1}/g" ${mrc_work_dir_updater}/updater-script
  sed -i "s/#DEVICECHK/${devicechk}/g" ${mrc_work_dir_updater}/updater-script
  sed -i '/#SYM/ r symlinks' ${mrc_work_dir_updater}/updater-script
  if [[ -f ${mrc_work_dir_project}/set_metadataV ]]; then
    cat ${mrc_work_dir_project}/set_metadataV >>${permtype}
  fi
  sed -i "/#PERM/ r ${permtype}" ${mrc_work_dir_updater}/updater-script
  sed -i "/#RADIO/ r radioscript" ${mrc_work_dir_updater}/updater-script
  grep -v "#PERM\|#SYM\|#ASSERT\|#RADIO" ${mrc_work_dir_updater}/updater-script >${mrc_work_dir_updater}/updater-script2
  mv ${mrc_work_dir_updater}/updater-script2 ${mrc_work_dir_updater}/updater-script
  rm -rf ${mrc_work_dir_project}/exdirs
  cd ${mrc_work_dir} || exit
  if [[ $(ls | grep "\.img$" | grep -v "system\|cache\|boot\|recovery\|preloader\|firstboot\|secondboot\|trustzone") ]]; then
    for part in $(ls | grep "\.img$" | grep -v "system\|cache\|boot\|recovery\|preloader\|firstboot\|secondboot\|trustzone"); do
      eximg=$(echo "${part}" | sed 's/\.img//')
      echo "${eximg}" >>${mrc_work_dir_project}/exdirs
    done
    for line in $(ls | grep "\.img$" | grep -v "system\|cache\|boot\|recovery\|preloader\|firstboot\|secondboot\|trustzone"); do
      line2=$(echo "${line}" | sed 's/\.img//')
      whatimg="${line2}"
      add_partition
    done
  fi
  mkdir -p ${mrc_work_dir}/verity
  cp ${mrc_device_patches_dir}/main/Verity.zip ${mrc_work_dir}/verity/Verity.zip
  cd ${mrc_work_dir} || exit
  rm -rf system.img vendor.img
}
