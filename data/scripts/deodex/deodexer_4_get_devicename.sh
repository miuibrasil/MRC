#!/bin/bash
# MRC - MIUI ROM Compiler
# SuperR work for MIUI Brasil
# Deodex script - Gets device name

get_devicename() {
  cd ${mrc_work_dir} || exit
  devicename=""
  while read i; do
    if [[ ! -f ${i} ]]; then
      continue
    fi
    while read x; do
      if [[ $(grep "${x}=" ${i}) ]]; then
        devicename="$(grep -m 1 "${x}=" ${i} | cut -d"=" -f2)"
        devicechk="${x}"
        break
      fi
    done <<< "$(echo -e "ro.product.device\nro.build.product\nro.product.name")"
  done <<< "$(echo -e "${sysdir}/build.prop\n${mrc_work_dir}/build.prop")"
  if [[ ! "${devicename}" ]]; then
    while read i; do
      if [[ ! -f ${i} ]]; then
        continue
      fi
      devicename="$(grep -m 1 "ro.build.description=" ${i} | cut -d"=" -f2 | cut -d'-' -f1)"
      devicechk="ro.product.device"
      break
    done <<< "$(echo -e "${sysdir}/build.prop\n${mrc_work_dir}/build.prop")"
  fi
}
