#!/bin/bash
# MRC - MIUI ROM Compiler
# SuperR work for MIUI Brasil
# Deodex script - Finish zip creation

finish_deodex() {
  build_zip
  finish_build
}

# Finish build
finish_build() {
  cd ${mrc_work_dir} || exit
  echo "Moving deodexed zip into the compiler folder..."
  mv output.zip ${mrc_deodexed_rom}/miui_${logname}_${versioninfo}_deodexed_${rombase}.zip
  mv ${mrc_source_rom}/${sourcezip} ${mrc_source_done}/${sourcezip}
  rm -rf ${mrc_log_dir}/extramv-${logname}.txt
  cd ${mrc_system} || exit
}

# Build zip
build_zip() {
  cd ${mrc_work_dir} || exit
  exzipfiles="*.img *.bin META-INF firmware-update compatibility.zip install verity system vendor" # firmware-update removed
  echo "Building deodexed zip..."
  ${p7z} a -tzip -mx5 output.zip ${exzipfiles}
}
