deodex_start_p() {
  cd ${mrc_work_dir} || exit
  if [[ -f ${mrc_work_dir}/system/init.rc && -d ${mrc_work_dir}/system/system/app ]]; then # If extracted images are residing in subfolders, set variables acordingly
    sysdir="${mrc_work_dir}/system/system"
  else
    sysdir="${mrc_work_dir}/system"
  fi

  if [[ ! -d "${sysdir}" ]]; then
    echo ""
    echo "Directory not found:"
    echo "${sysdir}"
    echo ""
    exit
  fi

  RELEASE=$(grep "ro\.build\.version\.release=" ${sysdir}/build.prop)
  ANDROID_VERSION=${RELEASE:25}
  COUNTFILE=$(find ${sysdir}/app ${sysdir}/framework ${sysdir}/priv-app ${sysdir}/vendor -name "*.odex" -type f 2>/dev/null | wc -l)
  COUNTFILEAPK=$(find ${sysdir}/app ${sysdir}/priv-app ${sysdir}/vendor/app  -name "*.odex" -type f 2>/dev/null | wc -l)
  COUNTFILEJAR=$(find ${sysdir}/framework ${sysdir}/vendor/framework  -name "*.odex" -type f  2>/dev/null | wc -l)

  deodex_p
}

deodex_p() {
  arch=""
  arch2=""
  while read i; do
    if [[ -d ${framedir}/${i} ]]; then
      archtest="${i}"
      break
    fi
  done <<< "$(echo -e "arm64\nx86_64\narm\nx86")"
  if [[ $(echo ${archtest} | grep "64") ]]; then
    arch2test=$(echo "${archtest}" | sed 's/_64//; s/64$//')
  fi
  if [[ ${archtest} && -f ${framedir}/${archtest}/boot.oat ]]; then
    arch="${archtest}"
    if [[ ${arch2test} && -f ${framedir}/${arch2test}/boot.oat ]]; then
      arch2="${arch2test}"
    fi
    echo "Start time: $(date +"%T")"
    echo "Android: ${ANDROID_VERSION}"
    echo "Total files: ${COUNTFILE} (APK: ${COUNTFILEAPK}, JAR: ${COUNTFILEJAR})"
    echo "Architecture: ${arch}"
    echo "Architecture: ${arch2}"
  else
    echo "Unsupported architecture"
  fi
  dodeodex_p() {
    app=""
    for app in $(echo "${applist}"); do
      cd ${deoappdir}/${app}
      app2=$(ls | grep \.apk$ | head -n 1 | sed 's/\.apk$//')
      if [[ ! -d "${deoarch}" || ! -f "${app2}.apk" ]]; then
        continue
      fi
      if [[ ! $(${p7z} l ${app2}.apk | grep classes.dex) ]]; then
        echo "${GREEN}Deodexing ${app2}.apk...${NORMAL}"
        cd ${deoappdir}/${app}/${deoarch}
        ${vdexext} -i ${app2}.vdex >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
        if [[ -f ${app2}_classes.cdex ]]; then
          ls | grep -e ".*_classes.*cdex" | while read i; do
            ${cdexext} ${i} 1>> ${mrc_log_dir}/MRC-Deodex-${logname}.log 2>> ${mrc_log_dir}/MRC-Deodex-${logname}.log
            mv ${i}.new $(basename "${i}" .cdex).dex
            rm -f "${i}"
          done
        fi
        if [[ ! $(find . -name ${app2}_classes.dex 2>/dev/null) ]]; then
          ${vdexext} -i ${app2}.vdex --ignore-crc-error >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
          if [[ ! $(find . -name ${app2}_classes.dex 2>/dev/null) ]]; then
            echo "${deoappdir/$mrc_work_dir/}/${app}/${app2}.apk" >> ${mrc_log_dir}/MRC-deodex_fail_list-${logname}
            rm -rf ${app2}_classes*
            continue
          else
            echo "${deoappdir/$mrc_work_dir/}/${app}/${app2}.apk" >> ${mrc_log_dir}/MRC-deodex_crc_ignored-${logname}
          fi
        fi
        if [[ -f ${app2}_classes.cdex ]]; then
          ls | grep -e ".*_classes.*cdex" | while read i; do
            ${cdexext} ${i} 1>> ${mrc_log_dir}/MRC-Deodex-${logname}.log 2>> ${mrc_log_dir}/MRC-Deodex-${logname}.log
            mv ${i}.new $(basename "${i}" .cdex).dex
            rm -f "${i}"
          done
        fi
        ls | grep "_classes" | while read i; do
          mv "${i}" "$deoappdir/$app/$(echo ${i##*_})"
        done
        (${aapt} add -fk ${deoappdir}/${app}/${app2}.apk ${deoappdir}/${app}/classes*.dex 2>&1) >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
        rm -rf ${deoappdir}/${app}/oat ${deoappdir}/${app}/classes*.dex
      else
        echo "${app}.apk is already deodexed!"
        rm -rf ${deoappdir}/${app}/${deoarch}
      fi
    done
  }
  cd ${mrc_work_dir} || exit
  if [[ -f ${sysdir}/init.rc && -d ${sysdir}/system/app ]]; then
    odextmp=$(find system -name *.vdex 2>/dev/null | grep -v "system/system/framework/oat/\|system/system/framework/$arch/\|system/system/framework/$arch2/\|^system/system/app/\|^system/system/priv-app/\|system/system/vendor/framework" | rev | cut -d"/" -f4- | rev | sort -u)
  else
    odextmp=$(find system -name *.vdex 2>/dev/null | grep -v "system/framework/oat/\|system/framework/$arch/\|system/framework/$arch2/\|^system/app/\|^system/priv-app/\|system/vendor/framework" | rev | cut -d"/" -f4- | rev | sort -u)
  fi
  if [[ -d ${sysdir}/app && ${odextmp} ]]; then
    echo "Moving extra apps..."
    extraapp=""
    line=""
    for line in ${odextmp}; do
      if [[ $(basename "${line}" | grep "^\.") ]]; then
        newapp=$(ls ${line} | grep .apk | cut -d"." -f1)
        newappdir=$(echo "${line}" | rev | cut -d"/" -f2- | rev)/${newapp}
        mv ${line} ${newappdir}
        echo "${mrc_work_dir}/${newappdir} ${mrc_work_dir}/${line}" >> ${mrc_log_dir}/extramv-${logname}.txt
        line2=$(echo ${line} | sed 's:\/:\\/:g')
        newappdir2=$(echo ${newappdir} | sed 's:\/:\\/:g')
        line="${newappdir}"
        extraapp=$(basename ${line})
      else
        extraapp=$(basename ${line})
        echo "${sysdir}/app/${extraapp} ${mrc_work_dir}/${line}" >> ${mrc_log_dir}/extramv-${logname}.txt
      fi
      mv ${mrc_work_dir}/${line} ${sysdir}/app/${extraapp}
    done
  fi
  chimerao=$(find ${sysdir}/priv-app/PrebuiltGmsCore*/app_chimera -name *.odex 2>/dev/null)
  if [[ ${chimerao} ]]; then
    for i in $(echo "${chimerao}"); do
      aname=$(basename "${i}" | sed 's/\.odex//')
      dname=$(echo "${i}" | rev | cut -d'/' -f4- | rev)
      darch=$(echo "${i}" | rev | cut -d'/' -f2 | rev)
      mkdir -p ${sysdir}/app/${aname}/oat/${darch}
      mv ${i} ${sysdir}/app/${aname}/oat/${darch}/
      if [[ -f $(echo "${i}" | sed 's/\.odex$/\.vdex/') ]]; then
        mv $(echo "${i}" | sed 's/\.odex$/\.vdex/') ${sysdir}/app/${aname}/oat/${darch}/
      fi
      if [[ -f ${dname}/${aname}.apk ]]; then
        mv ${dname}/${aname}.apk ${sysdir}/app/${aname}/
      fi
      echo "${sysdir}/app/${aname}/${aname}.apk ${dname}/" >> ${mrc_log_dir}/extramv-${mrc_log_dir}.txt
    done
    rm -rf ${dname}/oat
  fi
  if [[ -d "${sysdir}/vendor/framework" ]]; then
    vframe=$(find ${sysdir}/vendor/framework -name *.jar 2>/dev/null)
    for i in ${vframe}; do
      nframe=$(basename ${i} | sed 's/\.jar$//')
      dframe=$(dirname ${i})
      flip=""
      if [[ -f ${dframe}/oat/${arch}/${nframe}.vdex ]]; then
        mv ${dframe}/oat/${arch}/${nframe}.vdex ${framedir}/oat/${arch}/
        flip="1"
      fi
      if [[ ${flip} ]]; then
        mv ${i} ${framedir}/
        echo "${framedir}/${nframe}.jar ${i}" >> ${mrc_log_dir}/extramv-${logname}.txt
      fi
    done
    rm -rf ${sysdir}/vendor/framework/oat
  fi
  echo "Starting app deodex"
  applist=$(ls ${appdir})
  app=""
  for app in $(echo "${applist}"); do
    if [[ ${arch2} && -d ${appdir}/${app}/oat/${arch} && -d ${appdir}/${app}/oat/${arch2} ]]; then
      if [[ ${arch} != ${arch2} ]]; then
        rm -rf ${appdir}/${app}/oat/${arch2}
      fi
    fi
  done
  deoappdir="${appdir}"
  deoarch="oat/${arch}"
  deoarch2="${arch}"
  dodeodex_p
  if [[ ${arch2} ]]; then
    deoarch="oat/${arch2}"
    deoarch2="${arch2}"
    dodeodex_p
  fi
  echo "Starting priv-app deodex"
  applist=$(ls ${privdir})
  privapp=""
  for privapp in $(echo "${applist}"); do
    if [[ ${arch2} && -d ${privdir}/${privapp}/oat/${arch} && -d ${privdir}/${privapp}/oat/${arch2} ]]; then
      if [[ ${arch} != ${arch2} ]]; then
        rm -rf ${privdir}/${privapp}/oat/${arch2}
      fi
    fi
  done
  deoappdir="${privdir}"
  deoarch="oat/${arch}"
  deoarch2="${arch}"
  dodeodex_p
  if [[ ${arch2} ]]; then
    deoarch="oat/${arch2}"
    deoarch2="${arch2}"
    dodeodex_p
  fi
  echo "Starting framework deodex"
  if [[ -f ${framedir}/boot.vdex ]]; then
    cd ${framedir} || exit
    cp -p --remove-destination *.vdex ${arch}
    cp -p --remove-destination *.vdex ${arch2}
    rm -f *.vdex
  fi
  for d in ${framedir}/${arch} ${framedir}/${arch2}; do
    cd ${d} || exit
    if [[ -f "boot.oat" && -f "boot.vdex" ]]; then
      bootclass=$(strings boot.oat | tr ' ' '\n' 2>/dev/null | grep -m 1 '\-\-dex\-location=' | cut -d'=' -f2-)
      if [[ ${bootclass} && ! -f ${bootclass}__classes.dex ]]; then
        nclass="$(echo "${bootclass}" | sed 's/\.jar$/\.vdex/')"
        mv boot.vdex boot-$(basename "${bootclass}" | sed 's/\.jar$/\.vdex/')
      fi
    fi
    find -name "boot-*.vdex" | while read i; do
      nname=$(echo "${i}" | sed 's/^\.\///; s/^boot-//; s/\.vdex$//')
      if [[ -f ${framedir}/${nname}_classes.dex ]]; then
        continue
      fi
      echo "Deodexing $(echo "${i}" | sed 's/^\.\///')..."
      mv "${i}" "${nname}.vdex"
      ${vdexext} -i "${nname}.vdex" >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
      if [[ ! $(ls | grep "_classes") ]]; then
        ${vdexext} -i "${nname}.vdex" --ignore-crc-error >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
        if [[ ! $(ls | grep "_classes") ]]; then
          echo "${d/$mrc_work_dir/}/${nname}.jar" >> ${mrc_log_dir}/MRC-deodex_fail_list-${logname}
          continue
        else
          echo "${d/$mrc_work_dir/}/${nname}.jar" >> ${mrc_log_dir}/MRC-deodex_crc_ignored-${logname}
        fi
      fi
      if [[ -f ${nname}_classes.cdex ]]; then
        ls | grep -e "${nname}_classes.*cdex" | while read i; do
          ${cdexext} ${i} 1>> ${mrc_log_dir}/MRC-Deodex-${logname}.log 2>> ${mrc_log_dir}/MRC-Deodex-${logname}.log
          mv ${i}.new $(basename "${i}" .cdex).dex
          rm -f "${i}"
        done
      fi
      ls | grep "${nname}_classes" | while read r; do
        mv "${r}" "${framedir}/$(echo "${r}" | sed 's/_classes/.jar:classes/')"
      done
    done
  done

  if [[ -d ${framedir}/oat/${arch} || -d ${framedir}/oat/${arch2} ]]; then
    for d in ${framedir}/oat/${arch} ${framedir}/oat/${arch2}; do
      cd "${d}" || exit
      ls | grep ".vdex$" | while read i; do
        nname=$(echo "${i}" | sed 's/\.vdex$//')
        if [[ -f ${framedir}/${nname}_classes.dex ]]; then
          continue
        fi
        echo "Deodexing ${i}"
        ${vdexext} -i "${i}" >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
        if [[ ! $(ls | grep "_classes") ]]; then
          ${vdexext} -i "${i}" --ignore-crc-error >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
          if [[ ! $(ls | grep "_classes") ]]; then
            echo "${d/$mrc_work_dir/}/${nname}.jar" >> ${mrc_log_dir}/MRC-deodex_fail_list-${logname}
            continue
          else
            echo "${d/$mrc_work_dir/}/${nname}.jar" >> ${mrc_log_dir}/MRC-deodex_crc_ignored-${logname}
          fi
        fi
        if [[ -f ${nname}_classes.cdex ]]; then
          ls | grep -e "${nname}_classes.*cdex" | while read i; do
            ${cdexext} ${i} 1>> ${mrc_log_dir}/MRC-Deodex-${logname}.log 2>> ${mrc_log_dir}/MRC-Deodex-${logname}.log
            mv ${i}.new $(basename "${i}" .cdex).dex
            rm -f "$i"
          done
        fi
        ls | grep "${nname}_classes" | while read r; do
          mv ${r} ${framedir}/$(echo "${r}" | sed 's/_classes/\.jar:classes/')
        done
      done
    done
    echo "Packing jar files"
    cd ${framedir} || exit
    ls | grep "jar:classes.dex" | sort | while read line; do
      jname=$(echo "${line}" | cut -d':' -f1)
      ls | grep "^${jname}:" | while read i; do
        jclass=$(echo "${i}" | cut -d':' -f2)
        mv "${i}" "${jclass}"
      done
      if [[ -f "classes.dex" ]]; then
        (${aapt} add -fk "${jname}" classes*.dex 2>&1) >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
      else
        echo "Erro: ${jname} não possui um arquivo classes.dex" >> $mrc_log_dir/MRC-Deodex-${logname}.log
      fi
      rm -rf classes*.dex
    done
    echo "Cleaning up"
    if [[ -s ${mrc_log_dir}/extramv-${logname}.txt ]]; then
      cat ${mrc_log_dir}/extramv-${logname}.txt | while read line; do
        mv ${line}
        if [[ $(echo "${line}" | grep "app_chimera") ]]; then
          rm -rf $(dirname $(echo "${line}" | gawk '{print $1}'))
        fi
      done
    fi
    rm -rf ${mrc_log_dir}/extramv-${logname}.txt
    rm -rf $(find ${sysdir} -type d -name "oat")
    rm -rf ${arch}
    if [[ ${arch2} ]]; then
      rm -rf ${arch2}
    fi
    if [[ -f ${mrc_log_dir}/deodex_fail_list-${logname} ]]; then
      echo "Hora de término: $(date +"%T")"
      echo "Arquivos com erro no deodex:"
      echo "$(cat ${mrc_log_dir}/deodex_fail_list-${logname})"
    else
      echo "Hora de término: $(date +"%T")"
      echo "Deodex concluído"
    fi
  else
    echo "No odex files in framework dir. ROM probably already deodexed."
  fi
}
