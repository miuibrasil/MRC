#!/bin/bash
# MRC - MIUI ROM Compiler
# SuperR work for MIUI Brasil
# Deodex script - Main deodexer

deodex_start_no() {
  cd ${mrc_work_dir} || exit
  if [[ -f ${sysdir}/odex.sqsh ]]; then
    sqshfile="odex.sqsh"
    deodex_sqsh
    if [[ -f ${sysdir}/odex1.sqsh ]]; then
      sqshfile="odex1.sqsh"
      deodex_sqsh
      if [[ -f ${sysdir}/odex2.sqsh ]]; then
        sqshfile="odex2.sqsh"
        deodex_sqsh
      fi
    fi
  elif [[ -f ${sysdir}/odex.app.sqsh ]]; then
    sqshfile="odex.app.sqsh"
    deodex_sqsh
    if [[ -f ${sysdir}/odex.priv-app.sqsh ]]; then
      sqshfile="odex.priv-app.sqsh"
      deodex_sqsh
    fi
    if [[ -f ${sysdir}/odex.framework.sqsh ]]; then
      sqshfile="odex.framework.sqsh"
      deodex_sqsh
    fi
    if [[ -f ${sysdir}/etc/product/orig.applications.sqsh ]]; then
      sqshfile="orig.applications.sqsh"
      deodex_sqsh
    fi
    grep -v "odex.app\|odex.priv-app\|odex.framework\|orig.applications" ${mrc_work_dir_updater}/updater-script > ${mrc_work_dir_updater}/updater-script2
    mv ${mrc_work_dir_updater}/updater-script2 ${mrc_work_dir_updater}/updater-script
    grep -v "odex.app\|odex.priv-app\|odex.framework\|orig.applications" ${mrc_work_dir_project}/symlinks > ${mrc_work_dir_project}/symlinks2
    mv ${mrc_work_dir_project}/symlinks2 ${mrc_work_dir_project}/symlinks
    grep -v "odex.app\|odex.priv-app\|odex.framework\|orig.applications" ${mrc_work_dir_project}/symlinks.orig > ${mrc_work_dir_project}/symlinks.orig2
    mv ${mrc_work_dir_project}/symlinks.orig2 ${mrc_work_dir_project}/symlinks.orig
  fi
  get_heapsize
  if [[ ${api} -eq "21" || ${api} -eq "22" ]]; then
    oat2dex=${mrc_bin_dir}/baksmali/oat2dex.jar
    dtype="l"
    deodex_no
  elif [[ ${api} -eq 23 && ! ${androidversion} = "N" ]]; then
    smali=${mrc_bin_dir}/baksmali/smali.jar
    baksmali=${mrc_bin_dir}/baksmali/baksmali.jar
    dtype="m2"
    deodex_no
  elif [[ ${api} -eq 23 || ${api} -eq 24 || ${api} -eq 25 ]] && [[ ${androidversion} != "O" ]]; then
    smali=${mrc_bin_dir}/baksmali/smali.jar
    baksmali=${mrc_bin_dir}/baksmali/baksmali.jar
    dtype="n2"
    deodex_no
  elif [[ ${api} -ge 25 ]]; then
    dtype="o"
    deodex_no
  else
    echo "The ROM file supplied for deodex isnt compatible with this compiler."
    echo "Please try again."
    echo "ROM supplied: ${sourcezip}"
    exit
  fi
}
deodex_no() {
  arch=""
	arch2=""
	while read i; do
		if [[ -d "${framedir}/${i}" ]]; then
      archtest="${i}"
      break
    fi
	done <<< "$(echo -e "arm64\nx86_64\narm\nx86")"
	if [[ $(echo "${archtest}" | grep "64") ]]; then
		arch2test=$(echo "${archtest}" | sed 's/_64//; s/64$//')
	fi
	if [[ ${archtest} && -f ${framedir}/${archtest}/boot.oat ]]; then
		arch="${archtest}"
		if [[ ${arch2test} && -f ${framedir}/${arch2test}/boot.oat ]]; then
			arch2="${arch2test}"
		fi
	fi
	if [[ ! -f ${framedir}/${arch}/boot.oat ]]; then
		echo "${RED}WARNING:${NORMAL}"
		echo "There is no boot.oat in this rom. It cannot be deodexed, or is already deodexed."
    return
	fi
  dodeodex_no() {
    app=""
		for app in $(echo "${applist}"); do
			cd "${deoappdir}/${app}" || exit
        	app2=$(basename "$(find -name *.apk 2>/dev/null | head -n 1 | sed 's/\.apk$//')" 2>/dev/null)
			if [[ ! -d ${deoarch} || ! -f ${app2}.apk ]]; then
				continue
			fi
			if [[ ! $(${p7z} l ${app2}.apk | grep classes.dex) ]]; then
				echo -e "Deodexing ${GREEN}${app}${NORMAL}...\n"
				if [[ ${dtype} = "m2" || ${dtype} = "n2" ]]; then
					classes=$(java -Xmx${heapsize}m -jar ${baksmali} list dex ${deoarch}/${app2}.odex 2>&1)
					echo "${classes}" >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
					echo "${classes}" | while read line; do
						apkdex=$(basename $(echo "${line}"))
						if [[ ! $(echo "${apkdex}" | grep classes) ]]; then
							dexclass="classes.dex"
						else
							dexclass=$(echo "${apkdex}" | cut -d":" -f2-)
						fi
						java -Xmx${heapsize}m -jar ${baksmali} deodex -b ${framedir}/${deoarch2}/boot.oat ${deoarch}/${app2}.odex/${apkdex} -o ${deoarch}/smali
						java -Xmx${heapsize}m -jar ${smali} assemble -a ${api} ${deoarch}/smali -o ${deoarch}/${dexclass} >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
						rm -rf ${deoarch}/smali
						if [[ ! -f ${deoarch}/${dexclass} ]]; then
							echo "${deoappdir/$mrc_work_dir/}/${app}/${deoarch}/${dexclass}" >> ${mrc_log_dir}/deodex_fail-${logname}.log
							continue
						fi
					done
				elif [[ ${dtype} = "l" ]]; then
					java -Xmx${heapsize}m -jar ${oat2dex} ${deoarch}/${app2}.odex ${framedir}/${deoarch2}/odex >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
					if [[ ! -f ${deoarch}/${app2}.dex ]]; then
						rm -rf ${deoarch}/${app2}-classes*.dex
						continue
					fi
					mv ${deoarch}/${app2}.dex ${deoarch}/classes.dex
					find ${deoarch} -name ${app2}-classes*.dex | while read line; do
						appclassdex=$(basename ${line})
						appclassdir=$(dirname ${line})
						classname=$(echo "${appclassdex}" | cut -d"-" -f2)
						mv ${line} ${appclassdir}/${classname}
					done
				elif [[ ${dtype} = "o" ]]; then
					cd ${deoarch} || exit
					${vdexext2} -i ${app2}.vdex >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
					if [[ ! -f ${app2}_classes.dex ]]; then
						${vdexext2} -i ${app2}.vdex --ignore-crc-error >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
						if [[ ! -f ${app2}_classes.dex ]]; then
							echo "${deoappdir/$mrc_work_dir/}/${app}/${app2}.apk" >> ${mrc_log_dir}/deodex_fail-${logname}.log
							rm -rf ${app2}_classes*
							continue
						else
							echo "${deoappdir/$mrc_work_dir/}/${app}/${app2}.apk" >> ${mrc_log_dir}/deodex_crc_ignored-${logname}.log
						fi
					fi

					ls | grep "_classes" | while read i; do
						mv "${i}" "$(echo ${i##*_})"
					done
					cd ${deoappdir}/${app} || exit
				fi
				(${aapt} add -fk "${app2}.apk" "${deoarch}"/classes*.dex 2>&1) >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
				if [[ ${dtype} = "l" ]]; then
					rm -rf ${deoarch}
				else
					rm -rf oat
				fi
			else
				echo -e "${GREEN}${app}${NORMAL} is already deodexed...\n"
				if [[ ${dtype} != "l" ]]; then
					rm -rf ${deoappdir}/${app}/oat
				else
					rm -rf ${deoappdir}/${app}/${deoarch}
				fi
			fi
		done
  }
  rm -rf ${mrc_work_dir_project}/deodex_*
	touch ${mrc_work_dir_project}/deodex_$dtype
	if [[ $(find ${appdir} ${privdir} ${framedir} -name *odex.* 2>/dev/null | grep ".gz\|.xz") ]]; then
		echo -e "Extracting odex files...\n"
		find ${appdir} ${privdir} ${framedir} -name *odex.gz 2>/dev/null | while read line; do
			gzdir=$(dirname ${line})
			gzfile=$(basename ${line})
			echo -e "Extracting ${GREEN}${gzfile}${NORMAL}...\n"
			${p7z} e -o${gzdir} ${line} 2>/dev/null >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
		done
		find ${appdir} ${privdir} ${framedir} -name *odex.xz 2>/dev/null | while read line; do
			xzdir=$(dirname ${line})
			xzfile=$(basename ${line})
			echo -e "Extracting ${GREEN}${xzfile}${NORMAL}...\n"
			${p7z} e -o${xzdir} ${line} 2>/dev/null >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
		done
	fi
	if [[ ${dtype} != "l" ]]; then
		if [[ -f ${mrc_work_dir}/system/init.rc && -d ${mrc_work_dir}/system/system/app ]]; then
			odextmp=$(find system vendor -name *.odex 2>/dev/null | grep -v "system/system/framework/oat/\|system/system/framework/${arch}/\|system/system/framework/${arch2}/\|^system/system/app/\|^system/system/priv-app/\|system/system/vendor/framework" | rev | cut -d"/" -f4- | rev | sort -u)
		else
			odextmp=$(find system vendor -name *.odex 2>/dev/null | grep -v "system/framework/oat/\|system/framework/${arch}/\|system/framework/${arch2}/\|^system/app/\|^system/priv-app/\|system/vendor/framework" | rev | cut -d"/" -f4- | rev | sort -u)
		fi
	else
		odextmp=$(find system vendor -name *.odex 2>/dev/null | grep -v "system/framework/${arch}/\|system/framework/${arch2}/\|^system/app/\|^system/priv-app/" | rev | cut -d"/" -f3- | rev | sort -u)
	fi
	if [[ -d ${sysdir}/app && ${odextmp} ]]; then
		echo -e "Moving extra apps...\n"
		extraapp=""
		line=""
		for line in ${odextmp}; do
			if [[ $(basename "${line}" | grep "^\.") ]]; then
				newapp=$(ls ${line} | grep .apk | cut -d"." -f1)
				newappdir=$(echo "${line}" | rev | cut -d"/" -f2- | rev)/${newapp}
				mv ${line} ${newappdir}
				echo "${workdir}/${newappdir} ${workdir}/${line}" >> ${mrc_log_dir}/extramv-${logname}.txt
				line2=$(echo ${line} | sed 's:\/:\\/:g')
				newappdir2=$(echo ${newappdir} | sed 's:\/:\\/:g')
				line="${newappdir}"
				extraapp=$(basename ${line})
			else
				extraapp=$(basename ${line})
				echo "${sysdir}/app/${extraapp} ${mrc_work_dir}/${line}" >> ${mrc_log_dir}/extramv-${logname}.txt
			fi
			mv ${mrc_work_dir}/${line} ${sysdir}/app/${extraapp}
		done
	fi
	chimerao=$(find ${sysdir}/priv-app/PrebuiltGmsCore/app_chimera -name *.odex 2>/dev/null | grep "${arch}")
	chimerav=$(find ${sysdir}/priv-app/PrebuiltGmsCore/app_chimera -name *.vdex 2>/dev/null | grep "${arch}")
	if [[ ${chimerao} ]]; then
		for i in $(echo "${chimerao}"); do
			aname=$(basename ${i} | sed 's/\.odex//')
			dname=$(echo "${i}" | rev | cut -d"/" -f4)
			mkdir -p ${sysdir}/app/${aname}/oat/${arch}
			mv ${i} ${sysdir}/app/${aname}/oat/${arch}/
			if [[ ${chimerav} ]]; then
				if [[ -f $(echo "${i}" | sed 's/\.odex$/\.vdex/') ]]; then
					mv $(echo "${i}" | sed 's/\.odex$/\.vdex/') ${sysdir}/app/${aname}/oat/${arch}/
				fi
			fi
			mv ${sysdir}/priv-app/PrebuiltGmsCore/app_chimera/${dname}/${aname}.apk ${sysdir}/app/${aname}/
			echo "${sysdir}/app/${aname}/${aname}.apk ${sysdir}/priv-app/PrebuiltGmsCore/app_chimera/${dname}/" >> ${mrc_log_dir}/extramv-${logname}.txt
		done
		rm -rf ${sysdir}/priv-app/PrebuiltGmsCore/app_chimera/${dname}/oat
	fi
	if [[ -d "${sysdir}/vendor/framework" ]]; then
		vframe=$(find ${sysdir}/vendor/framework -name *.jar 2>/dev/null)
		for i in ${vframe}; do
			nframe=$(basename ${i} | sed 's/\.jar$//')
			dframe=$(dirname ${i})
			flip=""
			if [[ -f ${dframe}/oat/${arch}/${nframe}.odex ]]; then
				mv ${dframe}/oat/${arch}/${nframe}.odex ${framedir}/oat/${arch}/
				flip="1"
			fi
			if [[ -f ${dframe}/oat/${arch}/${nframe}.vdex ]]; then
				mv ${dframe}/oat/${arch}/${nframe}.vdex ${framedir}/oat/${arch}/
				flip="1"
			fi
			if [[ ${flip} ]]; then
				mv ${i} ${framedir}/
				echo "${framedir}/${nframe}.jar ${i}" >> ${mrc_log_dir}/extramv-${logname}.txt
			fi
		done
		rm -rf ${sysdir}/vendor/framework/oat
	fi
	if [[ ${dtype} = "l" ]]; then
		echo -e "Deoptimizing boot.oat...\n"
		if [[ ! -d "${framedir}/${arch}/odex" ]]; then
			java -Xmx${heapsize}m -jar ${oat2dex} boot ${framedir}/${arch}/boot.oat >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
		fi
		if [[ ${arch2} ]]; then
			if [[ ! -d "${framedir}/${arch2}/odex" ]]; then
				java -Xmx${heapsize}m -jar ${oat2dex} boot ${framedir}/${arch2}/boot.oat >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
			fi
		fi
	fi
	echo -e "Starting deodex on ${BLUE}system/app${NORMAL}...\n"
	applist=$(ls ${appdir})
	app=""
	for app in $(echo "${applist}"); do
		if [[ ${dtype} = "l" ]]; then
			if [[ ${arch2} && -d ${appdir}/${app}/${arch} && -d ${appdir}/${app}/${arch2} ]]; then
				rm -rf ${appdir}/${app}/${arch2}
			fi
		else
			if [[ ${arch2} && -d ${appdir}/${app}/oat/${arch} && -d ${appdir}/${app}/oat/${arch2} ]]; then
				rm -rf ${appdir}/${app}/oat/${arch2}
			fi
		fi
	done
	deoappdir="${appdir}"
	if [[ ${dtype} != "l" ]]; then
		deoarch="oat/${arch}"
		deoarch2="${arch}"
		dodeodex_no
		if [[ ${arch2} ]]; then
			deoarch="oat/${arch2}"
			deoarch2="${arch2}"
			dodeodex_no
		fi
	else
		deoarch="${arch}"
		deoarch2="${arch}"
		dodeodex_no
		if [[ ${arch2} ]]; then
			deoarch="${arch2}"
			deoarch2="${arch2}"
			dodeodex_no
		fi
	fi
	echo -e "Starting deodex on ${BLUE}system/priv-app${NORMAL}..."
	applist=$(ls ${privdir})
	privapp=""
	for privapp in $(echo "${applist}"); do
		if [[ ${dtype} != "l" ]]; then
			if [[ ${arch2} && -d ${privdir}/${privapp}/oat/${arch} && -d ${privdir}/${privapp}/oat/${arch2} ]]; then
				rm -rf ${privdir}/${privapp}/oat/${arch2}
			fi
		else
			if [[ ${arch2} && -d ${privdir}/${privapp}/${arch} && -d ${privdir}/${privapp}/${arch2} ]]; then
				rm -rf ${privdir}/${privapp}/${arch2}
			fi
		fi
	done
	deoappdir="${privdir}"
	if [[ ${dtype} != "l" ]]; then
		deoarch="oat/${arch}"
		deoarch2="${arch}"
		dodeodex_no
		if [[ ${arch2} ]]; then
			deoarch="oat/${arch2}"
			deoarch2="${arch2}"
			dodeodex_no
			deoarch="oat/${arch}"
			deoarch2="${arch}"
		fi
	else
		deoarch="${arch}"
		deoarch2="${arch}"
		dodeodex_no
		if [[ ${arch2} ]]; then
			deoarch="${arch2}"
			deoarch2="${arch2}"
			dodeodex_no
			deoarch="${arch}"
			deoarch2="${arch}"
		fi
	fi
  echo -e "Starting deodex on ${BLUE}system/framework${NORMAL}...\n"
	cd "${framedir}" || exit
	if [[ ${dtype} = "n2" || ${dtype} = "m2" ]]; then
		if [[ ${dtype} = "n2" ]]; then
			ls ${deoarch2} | grep .oat$ | sort | while read line; do
				if [[ ${line} != "boot.oat" ]]; then
					framejar=$(echo "$(echo "${line}" | sed 's/^boot-//; s/\.oat$//').jar")
					if [[ $(${p7z} l ${framejar} | grep classes.dex) ]]; then
						echo "${GREEN}${frame} is already deodexed...${NORMAL}"
						continue
					fi
				fi
				echo -e "Deodexing ${GREEN}${line}${NORMAL}...\n"
				classes=$(java -Xmx${heapsize}m -jar ${baksmali} list dex ${deoarch2}/${line} | rev | cut -d"/" -f1 | rev)
				echo "${classes}" >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
				echo "${classes}" | while read line2; do
					if [[ ! $(echo "${line2}" | grep classes) ]]; then
						line3=$(echo "${line2}" | rev | cut -d"." -f2- | rev)
						dexclass="classes.dex"
					else
						line3=$(echo "${line2}" | rev | cut -d"." -f3- | rev)
						dexclass=$(echo "${line2}" | cut -d":" -f2-)
					fi
					java -Xmx${heapsize}m -jar ${baksmali} deodex -b ${deoarch2}/boot.oat ${deoarch2}/${line}/${line2} -o ${deoarch2}/smali >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
					java -Xmx${heapsize}m -jar ${smali} assemble -a ${api} ${deoarch2}/smali -o ${line3}.jar:${dexclass} >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
					rm -rf ${deoarch2}/smali
				done
			done
		elif [[ ${dtype} = "m2" ]]; then
			classes=$(java -Xmx${heapsize}m -jar ${baksmali} list dex ${deoarch2}/boot.oat | rev | cut -d"/" -f1 | rev)
			line=""
			for line in $(echo "${classes}"); do
				if [[ ! $(echo "${line}" | grep classes) ]]; then
					framejar="${line}"
					dexclass=":classes.dex"
				else
					framejar=$(echo "${line}" | cut -d":" -f1)
					dexclass=""
				fi
				if [[ $(${p7z} l ${framejar} | grep classes.dex) ]]; then
					echo -e "${GREEN}${frame}${NORMAL} is already deodexed...\n"
					continue
				fi
				line2=$(echo "${line}" | rev | cut -d"." -f2- | rev)
				echo -e "Deodexing ${GREEN}${line2}${NORMAL}...\n"
				java -Xmx${heapsize}m -jar ${baksmali} deodex -b ${deoarch2}/boot.oat ${deoarch2}/boot.oat/${line} -o smali >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
				java -Xmx${heapsize}m -jar ${smali} assemble -a ${api} smali -o ${line}${dexclass} >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
				rm -rf smali
			done
		fi
		if [[ -d ${deoarch} ]]; then
			frame=""
			for frame in $(ls ${deoarch} | grep .odex$ | sort); do
				framejar=$(echo "$(echo "${frame}" | rev | cut -d"." -f2- | rev).jar")
				if [[ $(${p7z} l ${framejar} | grep classes.dex) ]]; then
					echo -e "${GREEN}${frame}${NORMAL} is already deodexed...\n"
					continue
				fi
				echo -e "Deodexing ${GREEN}${frame}${NORMAL}...\n"
				classes=$(java -Xmx${heapsize}m -jar ${baksmali} list dex ${deoarch}/${frame})
				#echo "${classes}" >> ${mrc_log_dir}/deodex-${logname}.log
				echo "${classes}" | while read line; do
					apkdex=$(basename $(echo "${line}"))
					if [[ ! $(echo "${apkdex}" | grep classes) ]]; then
						dexclass="${apkdex}:classes.dex"
					else
						dexclass="${apkdex}"
					fi
					java -Xmx${heapsize}m -jar ${baksmali} deodex -b ${deoarch2}/boot.oat ${deoarch}/${frame}/${apkdex} -o ${deoarch}/smali >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
					java -Xmx${heapsize}m -jar ${smali} assemble -a ${api} ${deoarch}/smali -o ${dexclass} >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
					rm -rf ${deoarch}/smali
				done
			done
		fi
	elif [[ ${dtype} = "l" ]]; then
		if [[ -d ${deoarch} ]]; then
			for frame in $(ls ${deoarch} | grep .odex$ | sort); do
				echo -e "Deodexing ${GREEN}${frame}${NORMAL}...\n"
				errtest=$(echo "${frame}" | rev | cut -d"." -f2- | rev)
				java -Xmx${heapsize}m -jar ${oat2dex} ${deoarch}/${frame} ${deoarch2}/odex >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
			done
		fi
		mv ${deoarch2}/dex/* ${deoarch}/
		ls ${deoarch} | grep "\.dex$" | while read line; do
			if [[ ! $(echo "${line}" | grep classes) ]]; then
				frame=$(echo "$(echo "${line}" | sed 's/\.dex//').jar:classes.dex")
			else
				dexclass=$(echo "${line}" | rev | cut -d"-" -f1 | rev)
				frame=$(echo "$(echo "${line}" | sed "s/-${dexclass}//").jar:${dexclass}")
			fi
			mv ${deoarch}/${line} ${frame}
		done
	elif [[ ${dtype} = "o" ]]; then
		for d in ${framedir}/${arch} ${framedir}/${arch2}; do
			cd "${d}" || exit
			if [[ -f "boot.oat" && -f "boot.vdex" ]]; then
				bootclass=$(strings boot.oat | tr ' ' '\n' 2>/dev/null | grep -m 1 '\-\-dex\-location=' | cut -d'=' -f2-)
				if [[ ${bootclass} && ! -f ${bootclass}__classes.dex ]]; then
					mv boot.vdex boot-$(basename "${bootclass}" | sed 's/\.jar$/\.vdex/')
				fi
			fi
			find -name "boot-*.vdex" | while read i; do
				nname=$(echo "${i}" | sed 's/^\.\///; s/^boot-//; s/\.vdex$//')
				if [[ -f ${framedir}/${nname}.jar__classes.dex ]]; then
					continue
				fi
				echo -e "Deodexing ${GREEN}$(echo "$i" | sed 's/^\.\///')${NORMAL}...\n"
				mv "${i}" "${nname}.vdex"
				${vdexext2} -i "${nname}.vdex" >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
				if [[ ! $(ls | grep "${nname}_classes") ]]; then
					${vdexext2} -i "${nname}.vdex" --ignore-crc-error >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
					if [[ ! $(ls | grep "${nname}_classes") ]]; then
						echo "${d/$mrc_work_dir/}/${nname}.jar" >> ${mrc_log_dir}/deodex_fail-${logname}.log
						continue
					else
						echo "${d/$mrc_work_dir/}/${nname}.jar" >> ${mrc_log_dir}/deodex_crc_ignored-${logname}.log
					fi
				fi
				ls | grep "${nname}.apk" | while read r; do
					mv "${r}" "${framedir}/$(echo "${r}" | sed 's/apk_/jar:/')"
				done
			done
		done
		for d in ${framedir}/oat/${arch} ${framedir}/oat/${arch2}; do
			cd "${d}" || exit
			ls | grep ".vdex$" | while read i; do
				nname=$(echo "${i}" | sed 's/\.vdex$//')
				if [[ -f ${framedir}/${nname}.jar__classes.dex ]]; then
					continue
				fi
				echo -e "Deodexing ${GREEN}${i}${NORMAL}...\n"
				${vdexext2} -i "${i}" >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
				if [[ ! $(ls | grep "_classes") ]]; then
					${vdexext2} -i "${i}" --ignore-crc-error >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
					if [[ ! $(ls | grep "_classes") ]]; then
						echo "${d/$mrc_work_dir/}/${nname}.jar" >> ${mrc_log_dir}/deodex_fail-${logname}.log
						continue
					else
						echo "${d/$mrc_work_dir/}/${nname}.jar" >> ${mrc_log_dir}/deodex_crc_ignored-${logname}.log
					fi
				fi
				ls | grep "${nname}.apk" | while read r; do
					mv ${r} ${framedir}/$(echo "${r}" | sed 's/apk_/jar:/')
				done
			done
		done
	fi
	echo -e "Packing dex into jarfiles...\n"
    cd "${framedir}" || exit
	ls | grep "jar:classes.dex" | sort | while read line; do
        jname=$(echo "${line}" | cut -d':' -f1)
		ls | grep "^${jname}:" | while read i; do
            jclass=$(echo "${i}" | cut -d':' -f2)
			mv "${i}" "${jclass}"
		done
        if [[ -f "classes.dex" ]]; then
			(${aapt} add -f -k -v "${jname}" classes*.dex 2>&1) >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
		else
			echo "ERROR: ${jname} has no classes.dex" >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
		fi
		rm -rf classes*.dex
	done
	if [[ ${dtype} = "l" || ${dtype} = "m" ]]; then
		rm -rf ${arch}/odex
		rm -rf ${arch}/dex
		if [[ ${arch2} ]]; then
			rm -rf ${arch2}/odex
			rm -rf ${arch2}/dex
		fi
	fi
	echo -e "Cleaning up...\n"
	if [[ -s ${mrc_log_dir}/extramv-${logname}.txt ]]; then
		cat ${mrc_log_dir}/extramv-${logname}.txt | while read line; do
			mv ${line}
			if [[ $(echo "${line}" | grep "app_chimera") ]]; then
				rm -rf $(dirname $(echo "${line}" | gawk '{print $1}'))
			fi
		done
	fi
	rm -rf oat
	rm -rf ${arch}
	if [[ ${arch2} ]]; then
		rm -rf ${arch2}
	fi

	if [[ ${dtype} = "l" ]]; then
		deodex_fail_list=$(grep "convertToDex: skip" ${mrc_log_dir}/MRC-Deodex-${logname}.log | cut -d"/" -f2- | sort -u)
	else
		deodex_fail_list=$(cat ${mrc_log_dir}/deodex_fail-${logname}.log 2>/dev/null)
	fi
	if [[ ${deodex_fail_list} ]]; then
		echo -e "There were problems deodexing the following\nand will cause issues if you flash the ROM.\nYou have been warned."
		echo "${RED}${deodex_fail_list}${NORMAL}"
	fi
	cd ${workdir} || exit
	stillodexed=$(find system vendor -name *.odex 2>/dev/null)
	if [[ ! "${stillodexed}" ]]; then
		echo "Deodexing complete."
	else
		echo "The following odex files are still in your ROM:"
		echo "${YELLOW}${stillodexed}${NORMAL}"
	fi
  cd ${mrc_work_dir} || exit
}
deodex_sqsh() {
	echo "Extracting ${sqshfile}..."
	if [[ $(echo "${sqshfile}" | grep "priv-app\|odex.app\|framework") ]]; then
		cd ${sysdir} || exit
		sqshtype=$(echo "${sqshfile}" | cut -d"." -f2)
		sqshdir=$(echo "sqshtmp/${sqshtype}")
	elif [[ $(echo "${sqshfile}" | grep "orig.applications") ]]; then
		cd ${sysdir}/etc/product || exit
		mkdir -p sqshtmp
		mv ${sqshfile} sqshtmp/
		cd sqshtmp || exit
		${p7z} x ${sqshfile} 2>/dev/null >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
		echo "Moving odex files..."
		if [[ $(ls | grep arm64) ]]; then
			odexarch="arm64"
			cd arm64 || exit
		elif [[ $(ls | grep arm) ]]; then
			odexarch="arm"
			cd arm || exit
		fi
		ls | while read line; do
			odexapp=$(echo "${line}" | sed 's/\.odex//')
			mv ${line} ${sysdir}/etc/product/applications/${odexapp}/oat/${odexarch}/
			mv ${sysdir}/etc/product/sqshtmp/${odexapp}/${odexapp}.apk ${sysdir}/etc/product/applications/${odexapp}/
		done
		rm -rf ${sysdir}/etc/product/sqshtmp ${sysdir}/etc/product/orig.applications
		sqshfile=""
		cd ${workdir} || exit
		return
	else
		sqshdir="sqshtmp"
	fi
	mkdir -p ${sqshdir}
	mv ${sqshfile} ${sqshdir}/
	cd ${sqshdir} || exit
	${p7z} x ${sqshfile} 2>/dev/null >> ${mrc_log_dir}/MRC-Deodex-${logname}.log
	rm -rf ${sqshfile}
	echo "Moving odex files..."
	if [[ -d ${sysdir}/${sqshdir}/arm || -d ${sysdir}/${sqshdir}/arm64 ]]; then
		if [[ -d ${sysdir}/${sqshdir}/arm ]]; then
			cd ${sysdir}/${sqshdir}/arm || exit
			sqsharch="arm"
		elif [[ -d ${sysdir}/${sqshdir}/arm64 ]]; then
			cd ${sysdir}/${sqshdir}/arm64 || exit
			sqsharch="arm64"
		fi
		if [[ ! ${sqshtype} = "framework" ]]; then
			line=""
			ls | grep .odex | while read line; do
				tmpapp=$(echo "${line}" | sed 's/\.odex//')
				mkdir -p ${sysdir}/${sqshtype}/${tmpapp}/oat/${sqsharch}
				mv "${line}" ${sysdir}/${sqshtype}/${tmpapp}/oat/${sqsharch}/
			done
		else
			line=""
			ls | grep .odex | while read line; do
				tmpapp=$(echo "${line}" | sed 's/\.odex//')
				if [[ -d ${framedir}/${tmpapp} ]]; then
					mkdir -p ${framedir}/${tmpapp}/oat/${sqsharch}
					mv "${line}" ${framedir}/${tmpapp}/oat/${sqsharch}/
				fi
			done
			mkdir -p ${sysdir}/${sqshtype}/oat/${sqsharch}
			mv * ${sysdir}/${sqshtype}/oat/${sqsharch}/
		fi
	else
		cd ${sysdir}/sqshtmp || exit
		line=""
		find . -type d | sed 's/^.\///' | while read line; do
			mkdir -p ${sysdir}/${line}
		done
		line=""
		find . -type f | sed 's/^.\///' | while read line; do
			mv ${line} ${sysdir}/${line}
		done
	fi
	cd $sysdir || exit
	rmleft=$(echo "${sqshfile}" | sed 's/\.sqsh$//')
	rm -rf sqshtmp ${rmleft}
	sqshfile=""
	cd ${mrc_work_dir} || exit
}

# Gets heapsize from system
get_heapsize() {
  if [[ -f ${mrc_config_dir}/.heapsize ]]; then
    heapsize=$(cat ${mrc_config_dir}/.heapsize)
  else
    heapsize=$(grep MemTotal /proc/meminfo | gawk '{ print $2/1024-500 }' | cut -d"." -f1)
  fi
}
