#!/bin/bash
# MRC - MIUI ROM Compiler
# SuperR work for MIUI Brasil
# Deodex script - Extract ROM img

img_extract() {
  cd ${mrc_work_dir} || exit
  ${simg2img} ${extractimg}.img ${extractimg}.img-2 2>/dev/null
  if [[ ! -s ${extractimg}.img-2 ]]; then
    rm -rf ${extractimg}.img-2
  else
    mv ${extractimg}.img-2 ${extractimg}.img
  fi
  mkdir -p output
  echo "Mounting ${extractimg} image..."
  ${usesudo}mount -o loop -t ext4 ${extractimg}.img output/
  mkdir -p ${extractimg}
  mkdir -p ${mrc_work_dir_project}
  echo "Copying ${extractimg} contents..."
  ${usesudo}cp -R output/* ${extractimg}/
  echo "Taking ownership of ${extractimg} files..."
  ${usesudo}chown -hR ${whoiam}:${whoiam} ${mrc_work_dir}/${extractimg}
  echo "Setting up permissions on ${extractimg} files..."
  ${usesudo}chmod -R a+rwX ${mrc_work_dir}/${extractimg}
  apitmp1=$(find system -name build.prop 2>/dev/null | grep -m 1 "")
  if [[ -f ${apitmp1} ]]; then
    apitmp=$(grep "ro.build.version.sdk" "${apitmp1}" | cut -d "=" -f2)
  else
    apitmp=""
  fi
  if [[ ${apitmp} -ge "19" ]]; then
    echo "Extracting metadatas from ${extractimg}..."
    local getmeta=$(${getmetadata} -s "${mrc_work_dir}/output" "${extractimg}")
    if [[ ${extractimg} == "system" || ${extractimg} == "vendor" ]]; then
      if [[ ${extractimg} == "system" ]]; then
        if [[ -f output/init.rc && -d output/system/app ]]; then
          echo "${getmeta}" | grep -v "system/system\|system/ " | while read i; do
            local thefile=$(echo "${i}" | gawk '{print $1}')
            local uid=$(echo "${i}" | gawk '{print $2}')
            local gid=$(echo "${i}" | gawk '{print $3}')
            local mode=$(echo "${i}" | gawk '{print $4}')
            local cap=$(echo "${i}" | gawk '{print $5}')
            local con=$(echo "${i}" | gawk '{print $6}')
            echo "set_metadata(\"/${thefile}\", \"uid\", ${uid}, \"gid\", ${gid}, \"mode\", ${mode}, \"capabilities\", ${cap}, \"selabel\", \"${con}\");" >>${mrc_work_dir_project}/root_meta
          done
          find output/system/bin -type f 2>/dev/null | sed 's/^output/system/' | sort >${mrc_work_dir_project}/binblk
          find output/system/vendor/bin -type f 2>/dev/null | sed 's/^output/system/' | sort >${mrc_work_dir_project}/vinblk
          cp ${mrc_scripts_deodex_dir}/convert_tools/set_meta_list2 ${mrc_work_dir_project}/set_meta_list
        else
          find output/bin -type f 2>/dev/null | sed 's/^output/system/' | sort >${mrc_work_dir_project}/binblk
          find output/vendor/bin -type f 2>/dev/null | sed 's/^output/system/' | sort >${mrc_work_dir_project}/vinblk
          cp ${mrc_scripts_deodex_dir}/convert_tools/set_meta_list ${mrc_work_dir_project}/set_meta_list
        fi
        sed -i "/#BIN/ r ${mrc_work_dir_project}/binblk" ${mrc_work_dir_project}/set_meta_list
        sed -i "/#VBIN/ r ${mrc_work_dir_project}/vinblk" ${mrc_work_dir_project}/set_meta_list
        rm -rf ${mrc_work_dir_project}/binblk ${mrc_work_dir_project}/vinblk
        metalist=$(cat ${mrc_work_dir_project}/set_meta_list)
      elif [[ ${extractimg} == "vendor" ]]; then
        if [[ ${getmeta} ]]; then
          metalist=$(echo "${getmeta}" | grep "^vendor/bin" | gawk '{print $1}' | sort)
        else
          metalist=$(cat ${mrc_scripts_deodex_dir}/convert_tools/set_meta_listV)
        fi
      fi
      echo "${metalist}" >${mrc_work_dir_project}/tmpmetalist
      echo "${getmeta}" | grep -v ":system_file\|:system_library_file\|:vendor_file\|vendor_configs_file\|vendor_app_file\|vendor_hal_file\|vendor_overlay_file\|vendor_framework_file\|same_process_hal_file\|:rootfs" | sort | while read line; do
        i=$(echo "${line}" | gawk '{print $1}')
        if [[ ! $(grep "${i}" ${mrc_work_dir_project}/tmpmetalist) ]]; then
          echo "${i}" >>${mrc_work_dir_project}/tmpmetalist
        fi
      done
      cat ${mrc_work_dir_project}/tmpmetalist | while read line; do
        if [[ ${extractimg} == "system" ]]; then
          set_metadata1="set_metadata1"
        else
          set_metadata1="set_metadataV"
        fi
        if [[ $(echo "${line}" | grep set_metadata) ]]; then
          ftmp=$(echo "${line}" | cut -d'"' -f2 | sed 's:^\/::')
          if [[ -f ${ftmp} || -d ${ftmp} ]]; then
            echo "${line}" >>${mrc_work_dir_project}/${set_metadata1}
          fi
        elif [[ -f ${line} ]]; then
          local i=$(grep "^${line} " <<<"${getmeta}")
          local thefile=$(echo "${i}" | gawk '{print $1}')
          local uid=$(echo "${i}" | gawk '{print $2}')
          local gid=$(echo "${i}" | gawk '{print $3}')
          local mode=$(echo "${i}" | gawk '{print $4}')
          local cap=$(echo "${i}" | gawk '{print $5}')
          local con=$(echo "${i}" | gawk '{print $6}')
          echo "set_metadata(\"/${thefile}\", \"uid\", ${uid}, \"gid\", ${gid}, \"mode\", ${mode}, \"capabilities\", ${cap}, \"selabel\", \"${con}\");" >>${mrc_work_dir_project}/${set_metadata1}
        fi
      done
    fi
  fi
  echo "Umounting ${extractimg} image..."
  ${usesudo}umount output/
  echo "Removing unneeded files..."
  rm -rf ${mrc_work_dir}/output ${mrc_work_dir_project}/cap_temp ${mrc_work_dir_project}/set_meta_list ${mrc_work_dir_project}/tmpmetalist
  extractimg=""
}
