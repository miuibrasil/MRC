#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Portions of code made by SuperR for MIUI Brasil
# Portions of code adapted from realtebo's work on MIUI.it compiler
# ROM Function - Patch kernel

patch_kernel() {
  unpack_boot
  fix_boot_img
    if [[ ${rombase} == "8.0" || ${rombase} == "8.1" || ${rombase} == "9.0" ]]; then
      sepol_patch
    fi
  repack_boot
}

# Unpacks boot image and copies it's contents
unpack_boot() {
  cd ${mrc_work_dir} || exit
  if [[ -f boot.img ]]; then
    mkdir -p ${mrc_work_dir}/bootimg
    cd ${mrc_bin_dir}/AIK || exit
    echo "Unpacking kernel..."
    if [[ ${whoiam} == "root" ]]; then
      ./unpackimg.sh ${mrc_work_dir}/boot.img 2>&1 >>${mrc_log_dir}/MRC-Deodex-${logname}.log
    else
      ./unpackimg.sh --nosudo ${mrc_work_dir}/boot.img 2>&1 >>${mrc_log_dir}/MRC-Deodex-${logname}.log
    fi
    if [[ ${?} == "1" ]]; then
      rm -rf ${mrc_work_dir}/bootimg ramdisk split_img
      echo "Error unpacking boot."
      bootext=""
      cd ${mrc_work_dir} || exit
      return
    fi
    ${usesudo}mv ramdisk ${mrc_work_dir}/bootimg/
    ${usesudo}mv split_img ${mrc_work_dir}/bootimg/
  else
    echo "Boot image wasn't found on the working directory"
    bootext=""
    cd ${mrc_work_dir} || exit
    return
  fi
  bootext=""
  cd ${mrc_work_dir} || exit
}

# Patches sepolicy on unpacked boot image
sepol_patch() {
  cd ${mrc_work_dir} || exit
  sepol=$(${usesudo}find -name *sepolicy* 2>/dev/null | grep -v "test\|txt\|mapping")
  if [[ ${sepol} ]]; then
    sepgood=""
    while read i; do
      omd5=$(md5sum "${i}" | gawk '{print $1}')
      if [[ $(echo "${i}" | grep "\.cil$") ]]; then
        line=$(grep -e "(allow zygote.* dalvikcache_data_file.* (file (" "${i}")
        if [[ ${line} ]]; then
          while read e; do
            if [[ ! $(echo "${e}" | grep "execute") ]]; then
              nline=$(echo "${e}" | sed 's/)))/\ execute)))/')
              sed -i "s/${e}/${nline}/" "${i}"
            fi
          done <<< "${line}"
        else
          znames=$(grep -e "(allow zygote.*" "${i}" | cut -d' ' -f2 | sort -u)
          dnames=$(grep -e "(allow .*dalvikcache_data_file.*" "${i}" | cut -d' ' -f3 | sort -u)
          if [[ ${znames} && ${dnames} ]]; then
            while read z; do
              while read d; do
                echo "(allow ${z} ${d} (file (execute)))" >>"${i}"
              done <<< "${dnames}"
            done <<< "${znames}"
          fi
        fi
      else
        dumpf=$(${usesudo}${selpatch} seinject -dt -P "${i}")
        znames=$(grep -e "ALLOW zygote.*" <<< "${dumpf}" | cut -d' ' -f3 | sort -u)
        dnames=$(grep -e "ALLOW.*dalvikcache_data_file.*" <<< "${dumpf}" | cut -d' ' -f5 | sort -u)
        if [[ ${znames} && ${dnames} ]]; then
          while read z; do
            while read d; do
              ${usesudo}${selpatch} seinject -s ${z} -t ${d} -c file -p execute -P "${i}" -o "${i}"
            done <<< "${dnames}"
          done <<< "${znames}"
        else
          ${usesudo}${selpatch} seinject -s zygote -t dalvikcache_data_file -c file -p execute -P "${i}" -o "${i}"
        fi
      fi
      if [[ $(md5sum "${i}" | gawk '{print $1}') != ${omd5} ]]; then
        sepgood=1
        ${usesudo}chown ${whoiam}:${whoiam} "${i}"
      fi
    done <<< "${sepol}"
    if [[ ${sepgood} ]]; then
      echo "Kernel sepolicy patched."
    else
      echo "Failed patching kernel sepolicy."
    fi
  else
    echo ""
  fi

  if [[ ${rombase} == "9.0" ]]; then
    cp -p -f ${mrc_device_patches_dir}/main/patchoat ${mrc_work_dir}/system/bin
  fi
}

# Repacks boot image
repack_boot() {
  if [[ ! -d ${mrc_work_dir_project}/boot_orig ]]; then
    mkdir -p ${mrc_work_dir_project}/boot_orig
    cp ${mrc_work_dir}/boot.img ${mrc_work_dir_project}/boot_orig/
  fi
  ${usesudo}mv ${mrc_work_dir}/bootimg/ramdisk ${mrc_bin_dir}/AIK/
  ${usesudo}mv ${mrc_work_dir}/bootimg/split_img ${mrc_bin_dir}/AIK/
  cd ${mrc_bin_dir}/AIK || exit
  echo "Repacking kernel..."
  ${usesudo}./repackimg.sh 2>&1 >>${mrc_log_dir}/MRC-Deodex-${logname}.log
  if [[ ${?} == "1" ]]; then
    ${usesudo}mv ${mrc_bin_dir}/AIK/ramdisk ${mrc_work_dir}/bootimg/
    ${usesudo}mv ${mrc_bin_dir}/AIK/split_img ${mrc_work_dir}/bootimg/
    echo "${RED}Failed repacking kernel image${NORMAL}"
  else
    ${usesudo}chown -hR ${whoiam}:${whoiam} image-new.img
    cp image-new.img ${mrc_work_dir}/boot.img
    ${verityoff} ${mrc_work_dir}/boot.img
    ${usesudo}./cleanup.sh
    rm -rf ${mrc_work_dir}/bootimg
  fi
  cd ${mrc_work_dir} || exit
  return
}

dm_force() {
  if [[ $(ls fstab.* 2>/dev/null) != "" ]]; then
    for fstabfile in fstab.*; do
      echo "Removing force encrypt and DM-Verity"
      if [[ "$(cat ${fstabfile} | grep ,verify)" != "" ]]; then
        ${usesudo}sed -i "s/,verify//g" ${fstabfile}
      fi
      if [[ "$(cat ${fstabfile} | grep fileencryption)" != "" ]]; then
        ${usesudo}sed -i "s/fileencryption/encryptable/g" ${fstabfile}
      fi
      if [[ "$(cat ${fstabfile} | grep forceencrypt)" != "" ]]; then
        ${usesudo}sed -i "s/forceencrypt/encryptable/g" ${fstabfile}
      fi
      if [[ "$(cat ${fstabfile} | grep forcedfeorfbe)" != "" ]]; then
        ${usesudo}sed -i "s/forcefdeorfbe/encryptable/g" ${fstabfile}
      fi
    done
  fi
}

# Boot image fixes
# Based on work by realtebo
fix_boot_img() {

  #RAM Disk Patcher
  echo "Patching ramdisk files"
  cd ${mrc_work_dir}/bootimg/ramdisk || exit
  # Disable setlockstate & enable service for selinux enforcement after boot
  if [[ -f init.miui.rc ]]; then
    start_line_number=$(grep -n -m 1 "# secureboot" init.miui.rc | cut -f1 -d:)
    end_line_number=$((${start_line_number} + 5 - 1))
    range="${start_line_number},${end_line_number}"
    sed -i -e "${range}"'d' init.miui.rc
    echo "Removing setlockstate lines ${range} from init.miui.rc"
    rm -rf sbin/setlockstate
    echo "Removing setlockstate binary from kernel"
  fi
  if [[ ${rombase} == "8.0" || ${rombase} == "8.1" || ${rombase} == "9.0" ]]; then
    cp ${mrc_device_patches_dir}/main/restart.sh sbin/restart.sh
    cat ${mrc_device_patches_dir}/main/init.miui.rc >> init.miui.rc
    sed -i "s/start miui-post-boot/start miui-post-boot\n    start restartc/g" init.miui.rc
    echo "Adding restartc service"
  fi
  # Disable force-encrypt & DM-Verity if there is a fstab embedded on the ramdisk
  dm_force
  # Changes default.prop file if the file is embedded on the ramdisk
  if [[ -f default.prop ]]; then
    # Remove secureboot prop
    if [[ $(cat default.prop | grep ro.secureboot.devicelock) != "" ]]; then
      echo "Removing ro.secureboot.devicelock"
      ${usesudo}sed -i "/ro.secureboot.devicelock/d" default.prop
    fi

    # Remove build fingerprint from boot image
    if [[ $(cat default.prop | grep ro.bootimage.build.fingerprint) != "" ]]; then
      echo "Removing ro.bootimage.build.fingerprint"
      ${usesudo}sed -i "/ro.bootimage.build.fingerprint/d" default.prop
    fi

    # Change usb default config to allow mtp & adb
    if [[ $(cat default.prop | grep persist.sys.usb.config) != "" ]]; then
      echo "Changing persist.sys.usb.config"
      ${usesudo}sed -i "/persist.sys.usb.config/d" default.prop
      ${usesudo}sed -i "$ a persist.sys.usb.config=mtp,adb" default.prop
    fi
  fi

  #Cmdline Patcher
  echo "Checking cmdline"
  cd ${mrc_work_dir}/bootimg/split_img || exit

  if [[ -f boot.img-cmdline ]]; then
    if [[ ${rombase} == "8.0" || ${rombase} == "8.1" || ${rombase} == "9.0" ]]; then
      sed -i "s/$/ androidboot.verifiedbootstate=green androidboot.hwc=GLOBAL androidboot.selinux=permissive/" boot.img-cmdline
    else
      sed -i "s/$/ androidboot.verifiedbootstate=green/" boot.img-cmdline
    fi
  fi

  if [[ ${devicename} == "meri" ]] || [[ ${devicename} == "song" ]]; then
    echo "Disabling AVB signature for meri/song"
    ${usesudo}rm -rf boot.img-avbtype
    ${usesudo}rm -rf boot.img-sigtype
  fi

  #Vendor Patcher
  if [[ -d ${mrc_work_dir}/vendor ]]; then
    cd ${mrc_work_dir}/vendor/etc || exit
    dm_force
  else
    cd ${mrc_work_dir}/system/vendor/etc || exit
    dm_force
  fi

  cd ${mrc_work_dir} || exit

}
