#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Load deodex plugins

pre_deodex_plugins() {
  # Load pre-deodex plugins
  if [[ $(ls ${mrc_device_patches_dir}/scripts/pre_deodex/ | grep .sh) != "" ]]; then
    for predeodex in ${mrc_device_patches_dir}/scripts/pre_deodex/*.sh; do
      . ${predeodex}
    done
  fi
}

post_deodex_plugins() {
  # Load post-deodex plugins
  if [[ $(ls ${mrc_device_patches_dir}/scripts/post_deodex/ | grep .sh) != "" ]]; then
    for postdeodex in ${mrc_device_patches_dir}/scripts/post_deodex/*.sh; do
      . ${postdeodex}
    done
  fi
}
