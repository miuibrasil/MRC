#!/bin/bash
#MRC Compiler
#Auto Upload ROMs

export autoupload="On"

# Autouploader menu item
autouploader() {
  if [[ ${autoupload} == "On" ]]; then
    echo "${GREEN}${autoupload}${NORMAL}"
  else
    echo "${RED}${autoupload}${NORMAL}"
  fi
}

autoupload_toggle() {
  if [[ ${autoupload} == "On" ]]; then
    export autoupload="Off"
  else
    export autoupload="On"
  fi
}

# Main auto upload function
autoupload() {
  if [[ ${autoupload} == "On" ]]; then
    afh_upload
  else
    echo "AutoUpload is currently disabled."
  fi
}

# Upload to Android File Host

afh_upload() {
  cd ${mrc_output_dir}/${romversion} || exit

  HOST='uploads.androidfilehost.com'
  USER='miuibrasil'
  PASSWD='MrkxY3NDl9m8'
  ftp -n -v ${HOST} <<EOF
    quote USER ${USER}
    quote PASS ${PASSWD}
    binary
    put ${projectname}_${romdevice}_${romversion}_${rom_md5}_${rombase}.zip
    size ${projectname}_${romdevice}_${romversion}_${rom_md5}_${rombase}.zip
    quit
EOF
}

# Upload to SourceForge
sf_upload() {
  cd ${mrc_output_dir}/${romversion} || exit

  USERNAME="miuiam-rom"
  HOST="frs.sourceforge.net"

  REMOPATH="/home/frs/project/miui-am"

  sftp ${USERNAME}@${HOST} <<EOF
    mkdir ${REMOPATH}/${romversion}
    cd ${REMOPATH}/${romversion}
    put ${projectname}_${romdevice}_${romversion}_${rom_md5}_${rombase}.zip
EOF
}
