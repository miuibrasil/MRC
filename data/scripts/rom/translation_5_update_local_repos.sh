#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Update local repositories

update_local_repositories() {
  header
  menutitle "Main Menu" "Translation tools" "Update local repositories"
  cd ${mrc_system} || exit
  run_ok "git pull" "Updating compiler"
  submodules_update
  echo -e "\nRepositories updated.\nTo start using the compiler with the changes made,\nrestart the compiler before doing anything else."
  footer
  presskey
}

submodules_update() {
  cd ${mrc_repositories_dir} || exit
  for repositories in *; do
    cd ${mrc_repositories_dir}/${repositories} || exit
    if [[ -d .git ]]; then
      run_ok "git pull" "Updating ${repositories}"
    else
      return
    fi
  done
  cd ${mrc_system} || exit
}
