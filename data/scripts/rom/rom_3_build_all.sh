#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Build all ROMs

build_all() {
  cd ${mrc_deodexed_rom} || exit
  for zipfile in *.zip; do
    cd ${mrc_deodexed_rom} || exit
    cleanup
    header
    menutitle "Main Menu" "Build ROMs" "Build all ROMs"
    echo "Current ROM: ${GREEN}${zipfile}${NORMAL}"
    unzip -qj ${zipfile} system/build.prop

    if [[ -f build.prop ]]; then

      export devicename2="$(cat build.prop | grep ro.product.device= | cut -d "=" -f2)"
      export romdevice="$(echo ${zipfile} | cut -d '_' -f2)"

      if [[ $(echo ${romdevice} | grep Global) == "" ]]; then
        export basetype="china"
      else
        export basetype="global"
      fi

      if [[ ${buildversion} == "Auto" ]]; then
        export romversion=$(echo "${zipfile}" | cut -d "_" -f3)
      else
        export romversion=${buildversion}
      fi

      export rombase="$(echo ${zipfile} | cut -d '_' -f5 | cut -c1-3)"

      if [[ ! -d ${mrc_output_dir}/${romversion} ]]; then
        mkdir -p ${mrc_output_dir}/${romversion}
      fi

    else
      echo "${RED}ERROR:${NORMAL}"
      echo "No build.prop was found on supplied zipfile ${zipfile}."
      echo "Make sure the ROM was properly deodexed before trying to build it."
      footer
      presskey
      build_roms
    fi

    mv ${zipfile} ${mrc_translator_input_dir}/input.zip

    ${usesudo}rm -rf ${mrc_translator_dir}/data/settings/hex.filelist.conf

    unzip -l ${mrc_translator_input_dir}/input.zip | grep system | grep "\.apk$\|\.jar$" | rev | cut -d " " -f1 | rev >> ${mrc_translator_dir}/data/settings/hex.filelist.conf

    if [[ "$(hostname)" != "miuibrasil" ]]; then
      export oldhostname="$(${usesudo}hostname)"
      ${usesudo}hostname miuibrasil
    fi

    RUN_LOG=${mrc_log_dir}/MRC-${romdevice}.log

    if [[ ${rombase} == "5.0" || ${rombase} == "5.1" || ${rombase} == "6.0" ]]; then
      ${translator} config ${mrc_config_dir}/translator/${basetype}6.conf
    else
      ${translator} config ${mrc_config_dir}/translator/${basetype}789.conf
    fi

    run_ok "${zipsigner} ${mrc_translator_output_dir}/output.zip ${mrc_output_dir}/${romversion}/output_signed.zip" "Signing ROM file"

    rm -rf ${mrc_translator_output_dir}/output.zip

    rom_md5="$(md5sum ${mrc_output_dir}/${romversion}/output_signed.zip | cut -d " " -f1 | cut -c1-10)"
    run_ok "mv ${mrc_output_dir}/${romversion}/output_signed.zip ${mrc_output_dir}/${romversion}/${projectname}_${romdevice}_${romversion}_${rom_md5}_${rombase}.zip" "Generating MD5 and moving ROM to output folder"

    mv ${mrc_translator_input_dir}/input.zip ${mrc_deodexed_done}/${zipfile}
    mv ${mrc_translator_dir}/logs/compiler-input.zip.log ${mrc_translator_dir}/logs/compiler-${romdevice}.log
    mv ${mrc_translator_dir}/logs/decompiler-input.zip.log ${mrc_translator_dir}/logs/decompiler-${romdevice}.log

    autoupload

    unset devicename2
    unset romdevice
    unset romversion
    unset rombase
    unset rom_md5

    cd ${mrc_deodexed_rom} || exit
    ${usesudo}rm -rf build.prop
  done
  header
  menutitle "Main Menu" "Build ROMs" "Build all ROMs"
  echo "ROMs compiled."
  echo "Compiled files are located in the ${BOLD}${mrc_output_dir}${NORMAL} directory."
  footer
  presskey
  cleanup
  cd ${mrc_system} || exit
}
