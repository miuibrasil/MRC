#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Build app pack for patches

create_app_packs() {
  header
  menutitle "Main Menu" "Translation tools" "Create app pack for patch testing"
  cd ${mrc_deodexed_rom} || exit
  mkdir -p ${mrc_output_dir}/apkpack
  for zipfile in *.zip; do
    run_ok "extract_patchable_apps" "Extracting patchable apps from ${zipfile}"
  done
  header
  menutitle "Main Menu" "Translation tools" "Create app pack for patch testing"
  echo "All files processed."
  echo "App packs for each deodexed rom are located in ${mrc_output_dir}."
  footer
  cleanup
  presskey
}

extract_patchable_apps() {
  cd ${mrc_deodexed_rom} || exit
  cleanup
  unzip -qjo ${zipfile} *.apk system/build.prop system/framework/framework.jar system/framework/mediatek-common.jar system/framework/services.jar -d ${mrc_work_dir}/temp
  cd ${mrc_work_dir}/temp || exit
  mkdir -p system/app
  mkdir -p system/framework
  mv build.prop system/
  cat ${mrc_config_dir}/apk_pack_patch | while read patchable_app; do
    if [[ ${patchable_app} == "framework-res.apk" || ${patchable_app} == "framework-ext-res.apk" || ${patchable_app} == "framework-miui-res.apk" || ${patchable_app} == "framework.jar" || ${patchable_app} == "mediatek-common.jar" || ${patchable_app} == "services.jar" ]]; then
      mv ${patchable_app} system/framework
    else
      appdir=$(echo ${patchable_app} | cut -d "." -f1)
      mkdir -p system/app/${appdir}
      mv ${patchable_app} system/app/${appdir}
    fi
  done
  rm -rf *.apk
  run_ok "zip -qr ${zipfile} system" "Building app pack for patch testing..."
  mv ${zipfile} ${mrc_output_dir}/apkpack/"$(echo ${zipfile} | cut -d "_" -f2)".zip
  cd ${mrc_work_dir} || exit
  rm -rf *
  cd ${mrc_deodexed_rom} || exit
}
