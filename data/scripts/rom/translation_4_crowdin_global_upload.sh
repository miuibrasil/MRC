#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Upload Global translations to Crowdin

upload_global_to_crowdin() {
  header
  menutitle "Main Menu" "Translation tools" "Upload Global translations to Crowdin"
  cd ${mrc_repositories_dir}/MRC-Global-XML

  echo -e "Starting XML upload...\n"
  ${crowdin} upload sources
  echo -e "\nXML files uploaded to Crowdin. To view them and translate new strings, open https://translate.miuibrasil.org in your browser."
  footer
  presskey
}
