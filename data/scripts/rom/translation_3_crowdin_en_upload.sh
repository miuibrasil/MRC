#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Upload English XMLs to Crowdin

upload_source_to_crowdin() {
  header
  menutitle "Main Menu" "Translation tools" "Upload English XML to Crowdin"
  cd ${mrc_repositories_dir}/MRC-Source-XML
  echo -e "Starting XML upload...\n"
  ${crowdin} upload sources
  echo -e "\nXML files uploaded to Crowdin. To view them and translate new strings, open https://translate.miuibrasil.org in your browser."
  footer
  presskey
}
