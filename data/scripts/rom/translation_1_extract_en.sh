#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Extract english XML from ROM

extract_english_xml() {
  cd ${mrc_deodexed_rom} || exit
  rm -rf ${mrc_repositories_dir}/MRC-Source-XML/Source 2>/dev/null
  for zipfile in *.zip; do
    cleanup
    header
    menutitle "Main Menu" "Translation tools" "Extract English XML files"
    echo "Processing file ${GREEN}${zipfile}${NORMAL}"
    mkdir -p ${mrc_work_dir}/appcache/ROM/system
    run_ok "unzip -oqj ${zipfile} *.apk system/build.prop -d ${mrc_work_dir}/appcache/ROM/system" "Extracting apps"
    cd ${mrc_work_dir}/appcache/ROM || exit
    devname="$(cat system/build.prop | grep ro.product.device= | cut -d "=" -f2)"
    export devname
    echo "${devname}"

    cat ${mrc_scripts_rom_dir}/xmlconf/frameworks | while read frame; do
      if [[ -f ${mrc_work_dir}/appcache/ROM/system/${frame} ]]; then
        ${apktool} if ${mrc_work_dir}/appcache/ROM/system/${frame} 2>/dev/null
      fi
    done

    if [[ -e ${mrc_scripts_rom_dir}/xmlconf/devices/${devname} ]]; then

      mrc_xmlextract_configfile="${mrc_scripts_rom_dir}/xmlconf/devices/${devname}"
      echo "Config file for ${devname} found, starting extraction"

      while IFS="|" read destfolder appname; do

        apk_name="$(basename ${appname})"
        export apk_name

        if [[ -f ${mrc_work_dir}/appcache/ROM/system/${appname} ]]; then

          rm -rf ${mrc_work_dir}/appcache/apk_wip
          run_ok "${apktool} decode -f -s ${mrc_work_dir}/appcache/ROM/system/${appname} -o ${mrc_work_dir}/appcache/apk_wip >>/dev/null" "Decoding ${apk_name}"

          for xmltype in $(cat ${mrc_scripts_rom_dir}/xmlconf/xmltypes); do
            if [[ -e ${mrc_work_dir}/appcache/apk_wip/res/values/${xmltype}.xml ]]; then
              sed -i '/APKTOOL_DUMMY/d' ${mrc_work_dir}/appcache/apk_wip/res/values/${xmltype}.xml
              mkdir -p ${mrc_repositories_dir}/MRC-Source-XML/Source/${destfolder}/${apk_name}/res/values
              cp ${mrc_work_dir}/appcache/apk_wip/res/values/${xmltype}.xml ${mrc_repositories_dir}/MRC-Source-XML/Source/${destfolder}/${apk_name}/res/values
            fi
          done
        fi
      done <${mrc_xmlextract_configfile}

    else
      echo "No apps found for selected device."
    fi

    cd ${mrc_deodexed_rom} || exit
  done
  presskey
}
