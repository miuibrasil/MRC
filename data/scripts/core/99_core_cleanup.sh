#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Cleanup script

# Cleans up environment
cleanup() {
  if [[ $(mount | grep system.img) != "" || $(mount | grep vendor.img) != "" ]]; then
    ${usesudo}umount ${mrc_work_dir}/output
  fi
  ${usesudo}rm -rf ${mrc_work_dir}/*
  ${usesudo}rm -rf ${mrc_deodexed_rom}/build.prop
  ${usesudo}rm -rf ${mrc_log_dir}/.stopspinning
  ${usesudo}rm -rf ${mrc_log_dir}/extramv*
  ${usesudo}rm -rf ${mrc_translator_dir}/projects/*
  ${usesudo}rm -rf ${mrc_translator_dir}/output/*.zip
  ${usesudo}rm -rf ${mrc_translator_dir}/input/*.zip
}

# Clean old logs on system startup
clean_logs() {
  ${usesudo}rm -rf ${mrc_log_dir}/*.log 2>/dev/null
  ${usesudo}rm -rf ${mrc_log_dir}/*.txt 2>/dev/null
}
