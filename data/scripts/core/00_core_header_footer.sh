#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Header and Footer for menus and submenus

# Menu header
header() {
  clear
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' =
  COLUMNS=$(tput cols)
  htitle="MIUI ROM Compiler - ${mrc_version} - $(date +%d/%m/%Y)"
  printf "%*s\n" $(((${#htitle} + $COLUMNS) / 2)) "${htitle}"
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' =
}

# Menu separator/title
menutitle() {
  if [[ ${3} != "" ]]; then
    echo "${BLUE}${1}${NORMAL} > ${BLUE}${2}${NORMAL} > ${BLUE}${3}${NORMAL}"
  else
    if [[ ${2} != "" ]]; then
      echo "${BLUE}${1}${NORMAL} > ${BLUE}${2}${NORMAL}"
    else
      echo "${BLUE}${1}${NORMAL}"
    fi
  fi
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' =
  echo ""
}

# Menu footer
footer() {
  echo ""
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' =
}

# Menu separator
separator() {
  printf '%*s\n' "${COLUMNS:-$(tput cols)}" '' | tr ' ' =
}

# Press key shortcut
presskey() {
  read -n 1 -s -r -p "Press any key to continue..."
}
