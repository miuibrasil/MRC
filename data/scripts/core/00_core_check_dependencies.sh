#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Checks system against any unsatisfied dependencies

cat ${mrc_config_dir}/.dependencies | while read deps; do
  if [[ -z "$(which ${deps})" ]]; then
    echo "${deps}" >>${mrc_config_dir}/.missingdeps
  fi
done

if [[ -f ${mrc_config_dir}/.missingdeps ]]; then
  clear
  echo -e "Your system is missing the following programs:\n"
  cat ${mrc_config_dir}/.missingdeps | while read missing; do
    echo -e "${RED}${missing}${NORMAL}"
  done
  rm -rf ${mrc_config_dir}/.missingdeps
  echo -e "\nInstall them using your system's package manager before proceeding."
  exit
fi
