#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Checks the full path where our compiler is located against space characters.

if [[ $(echo $(pwd) | grep " ") ]]; then
  echo -e "${RED}This compiler must be called in a path without spaces.${NORMAL}\n\n${BLUE}CURRENT PATH:${NORMAL}"
  echo "${YELLOW}$(pwd)${NORMAL}"
  rm -rf ${mrc_system}/data/config/.mrc
  exit
fi
