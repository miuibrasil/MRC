#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Checks user privileges

if [ "$(whoami)" == "root" ]; then
  export usesudo=""
else
  export usesudo="sudo "
fi
export whoiam="$(whoami)"
