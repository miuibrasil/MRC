#!/bin/bash
# MRC - MIUI ROM Compiler
# Spinner
# https://github.com/swelljoe/spinner

spincleanup() {
  if [ ! -z "$allpids" ]; then
    for pid in $allpids; do
      kill "$pid" 1>/dev/null 2>&1
    done
    tput rc
  fi
  tput cnorm
  return 1
}

trap spincleanup INT QUIT TERM EXIT

shell_has_unicode() {
  env printf "\\u2714" >unitest.txt
  read -r unitest <unitest.txt
  rm -f unitest.txt
  if [ ${#unitest} -le 3 ]; then
    return 0
  else
    return 1
  fi
}

SPINNER_COLORCYCLE=0
SPINNER_COLORNUM=6
if shell_has_unicode; then
  SPINNER_SYMBOLS="WIDE_UNI_GREYSCALE2"
else
  SPINNER_SYMBOLS="WIDE_ASCII_PROG"
fi
SPINNER_CLEAR=0
SPINNER_DONEFILE="${mrc_log_dir}/.stopspinning"

spinner() {
  local WIDE_ASCII_PROG="[>----] [=>---] [==>--] [===>-] [====>] [----<] [---<=] [--<==] [-<===] [<====]"
  local WIDE_UNI_GREYSCALE2="▒▒▒▒▒▒▒ █▒▒▒▒▒▒ ██▒▒▒▒▒ ███▒▒▒▒ ████▒▒▒ █████▒▒ ██████▒ ███████ ▒██████ ▒▒█████ ▒▒▒████ ▒▒▒▒███ ▒▒▒▒▒██ ▒▒▒▒▒▒█"
  local SPINNER_NORMAL
  SPINNER_NORMAL=$(tput sgr0)
  eval SYMBOLS=\$${SPINNER_SYMBOLS}
  SPINNER_PPID=$(ps -p "$$" -o ppid=)
  while :; do
    tput civis
    for c in ${SYMBOLS}; do
      if [ $SPINNER_COLORCYCLE -eq 1 ]; then
        if [ $SPINNER_COLORNUM -eq 7 ]; then
          SPINNER_COLORNUM=1
        else
          SPINNER_COLORNUM=$((SPINNER_COLORNUM + 1))
        fi
      fi
      local SPINNER_COLOR
      SPINNER_COLOR=$(tput setaf ${SPINNER_COLORNUM})
      tput sc
      env printf "${SPINNER_COLOR}${c}${SPINNER_NORMAL}"
      tput rc
      if [ -f "${SPINNER_DONEFILE}" ]; then
        if [ ${SPINNER_CLEAR} -eq 1 ]; then
          tput el
        fi
        rm -f ${SPINNER_DONEFILE}
        break 2
      fi
      env sleep .2
      if [ ! -z "$SPINNER_PPID" ]; then
        SPINNER_PARENTUP=$(ps --no-headers $SPINNER_PPID)
        if [ -z "$SPINNER_PARENTUP" ]; then
          break 2
        fi
      fi
    done
  done
  tput rc
  tput cnorm
  return 0
}

RUN_LOG="${mrc_log_dir}/MRC.log"

run_ok() {
  local cmd="${1}"
  local msg="${2}"
  local columns
  columns=$(tput cols)
  COL=$((${columns} - ${#msg} - 8))
  printf "%s%${COL}s" "$2"
  CHECK='\u2714'
  BALLOT_X='\u2718'
  spinner &
  spinpid=$!
  allpids="$allpids $spinpid"
  echo "Spin pid is: $spinpid" >>${RUN_LOG}
  eval "${cmd}" 1>>${RUN_LOG} 2>&1
  local res=$?
  touch ${SPINNER_DONEFILE}
  env sleep 0.3
  pidcheck=$(ps --no-headers ${spinpid})
  if [ ! -z "$pidcheck" ]; then
    echo "Made it here...why?" >>${RUN_LOG}
    kill $spinpid 2>/dev/null 2>&1
    rm -rf ${SPINNER_DONEFILE} 2>/dev/null 2>&1
    tput rc
    tput cnorm
  fi
  printf "${msg}: " >>${RUN_LOG}
  if shell_has_unicode; then
    if [ $res -eq 0 ]; then
      printf "Success.\\n" >>${RUN_LOG}
      env printf "${GREENBG}[  ${CHECK}  ]${NORMAL}\\n"
      return 0
    else
      env printf "${REDBG}[  ${BALLOT_X}  ]${NORMAL}\\n"
      return ${res}
    fi
  else
    if [ $res -eq 0 ]; then
      printf "Success.\\n" >>${RUN_LOG}
      env printf "${GREENBG}[ OK! ]${NORMAL}\\n"
      return 0
    else
      echo
      env printf "${REDBG}[ERROR]${NORMAL}\\n"
      return ${res}
    fi
  fi
  rm -rf ${SPINNER_DONEFILE} 2>/dev/null 2>&1
}
