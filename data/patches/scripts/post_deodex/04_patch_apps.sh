#!/bin/bash
#Smali patcher
#SuperR & ale8530

smali_patcher() {
  echo "Applying smali patches into ${logname} deodexed ROM..."
  cd ${mrc_patches_dir} || exit

  if [[ -f ${mrc_log_dir}/MRC-Patcher-${logname}.log ]]; then
    rm -f ${mrc_log_dir}/MRC-Patcher-${logname}.log
  fi

  if [[ ${rombase} == "6.0" ]]; then
    smaliapi="23"
  elif [[ ${rombase} == "7.0" ]]; then
    smaliapi="24"
  elif [[ ${rombase} == "7.1" ]]; then
    smaliapi="25"
  elif [[ ${rombase} == "8.0" ]]; then
    smaliapi="26"
  elif [[ ${rombase} == "8.1" ]]; then
    smaliapi="27"
  elif [[ ${rombase} == "9.0" ]]; then
    smaliapi="28"
  else
    echo "ROM API isn't supported by the smali patcher plugin."
    return
  fi

  if [[ ${logname} == "HM3S" || ${logname} == "HM3SGlobal" ]]; then
    patchfolder="land"
  elif [[ ${logname} == "HM4" || ${logname} == "HM4Global" ]]; then
    patchfolder="prada"
  elif [[ ${logname} == "HM4A" || ${logname} == "HM4AGlobal" ]]; then
    patchfolder="rolex"
  elif [[ ${logname} == "HM4Pro" || ${logname} == "HM4ProGlobal" ]]; then
    patchfolder="markw"
  elif [[ ${logname} == "HM4X" || ${logname} == "HM4XGlobal" ]]; then
    patchfolder="santoni"
  elif [[ ${logname} == "HM5A" || ${logname} == "HM5AGlobal" ]]; then
    patchfolder="riva"
  elif [[ ${logname} == "HM5" || ${logname} == "HM5Global" ]]; then
    patchfolder="rosy"
  elif [[ ${logname} == "HM5Plus" || ${logname} == "HM5PlusGlobal" ]]; then
    patchfolder="vince"
  elif [[ ${logname} == "HM6Pro" || ${logname} == "HM6ProINGlobal" ]]; then
    patchfolder="sakura"
  elif [[ ${logname} == "HMNote3Pro" || ${logname} == "HMNote3ProGlobal" ]]; then
    patchfolder="kenzo"
  elif [[ ${logname} == "HMNote3ProtwGlobal" ]]; then
    patchfolder="kate"
  elif [[ ${logname} == "HMNote4" || ${logname} == "HMNote4Global" ]]; then
    patchfolder="nikel"
  elif [[ ${logname} == "HMNote4X" || ${logname} == "HMNote4XGlobal" ]]; then
    patchfolder="mido"
  elif [[ ${logname} == "HMNote5A" || ${logname} == "HMNote5AGlobal" ]]; then
    patchfolder="ugg"
  elif [[ ${logname} == "HMNote5ALITE" || ${logname} == "HMNote5ALITEGlobal" ]]; then
    patchfolder="ugglite"
  elif [[ ${logname} == "HMNote5" || ${logname} == "HMNote5HMNote5ProGlobal" ]]; then
    patchfolder="whyred"
  elif [[ ${logname} == "HMS2" || ${logname} == "HMS2Global" ]]; then
    patchfolder="ysl"
  elif [[ ${logname} == "HMPro" ]]; then
    patchfolder="omega"
  elif [[ ${logname} == "MI4c" ]]; then
    patchfolder="libra"
  elif [[ ${logname} == "MI4s" ]]; then
    patchfolder="aqua"
  elif [[ ${logname} == "MI5" || ${logname} == "MI5Global" ]]; then
    patchfolder="gemini"
  elif [[ ${logname} == "MI5C" ]]; then
    patchfolder="song"
  elif [[ ${logname} == "MI5S" || ${logname} == "MI5SGlobal" ]]; then
    patchfolder="capricorn"
  elif [[ ${logname} == "MI5SPlus" || ${logname} == "MI5SPlusGlobal" ]]; then
    patchfolder="natrium"
  elif [[ ${logname} == "MI5X" ]]; then
    patchfolder="tiffany"
  elif [[ ${logname} == "MI6" || ${logname} == "MI6Global" ]]; then
    patchfolder="sagit"
  elif [[ ${logname} == "MI6X" || ${logname} == "MI6XGlobal" ]]; then
    patchfolder="wayne"
  elif [[ ${logname} == "MI8" || ${logname} == "MI8Global" ]]; then
    patchfolder="dipper"
  elif [[ ${logname} == "MI8Explorer" || ${logname} == "MI8ExplorerGlobal" ]]; then
    patchfolder="ursa"
  elif [[ ${logname} == "MI8SE" || ${logname} == "MI8SEGlobal" ]]; then
    patchfolder="sirius"
  elif [[ ${logname} == "MI8UD" || ${logname} == "MI8UDGlobal" ]]; then
    patchfolder="equuleus"
  elif [[ ${logname} == "MI8Lite" || ${logname} == "MI8LiteGlobal" ]]; then
    patchfolder="platina"
  elif [[ ${logname} == "MIMAX" || ${logname} == "MIMAXGlobal" ]]; then
    patchfolder="hydrogen"
  elif [[ ${logname} == "MIMAX652" || ${logname} == "MIMAX652Global" ]]; then
    patchfolder="helium"
  elif [[ ${logname} == "MIMAX2" || ${logname} == "MIMAX2Global" ]]; then
    patchfolder="oxygen"
  elif [[ ${logname} == "MIMAX3" || ${logname} == "MIMAX3Global" ]]; then
    patchfolder="nitrogen"
  elif [[ ${logname} == "MIMIX" || ${logname} == "MIMIXGlobal" ]]; then
    patchfolder="lithium"
  elif [[ ${logname} == "MIMIX2" || ${logname} == "MIMIX2Global" ]]; then
    patchfolder="chiron"
  elif [[ ${logname} == "MIMIX2S" || ${logname} == "MIMIX2SGlobal" ]]; then
    patchfolder="polaris"
  elif [[ ${logname} == "MIMIX3" || ${logname} == "MIMIX3Global" ]]; then
    patchfolder="perseus"
  elif [[ ${logname} == "MINote2" || ${logname} == "MINote2Global" ]]; then
    patchfolder="scorpio"
  elif [[ ${logname} == "MINote3" || ${logname} == "MINote3Global" ]]; then
    patchfolder="jason"
  fi

  echo "Patching common apps..."
  ${smalip} -w ${mrc_work_dir}/system -d ${mrc_smali_patches_dir}/00_common -a ${smaliapi} -b baksmali-2.2.5.jar -s smali-2.2.5.jar -l ${mrc_log_dir}/MRC-Patcher-${logname}.log >/dev/null

  echo "Patching ${logname} apps..."
  ${smalip} -w ${mrc_work_dir}/system -d ${mrc_smali_patches_dir}/${patchfolder} -a ${smaliapi} -b baksmali-2.2.5.jar -s smali-2.2.5.jar -l ${mrc_log_dir}/MRC-Patcher-${logname}.log >/dev/null

  cd ${mrc_work_dir} || exit

  echo "Patching complete."

}

smali_patcher
