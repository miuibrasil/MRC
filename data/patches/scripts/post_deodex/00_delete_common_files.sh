#!/bin/bash
# MRC - MIUI ROM Compiler
# adapted from realtebo's work on MIUI.it compiler
# Delete common files/bloatware

pl_delete_commonfiles(){
  cd ${mrc_work_dir} || exit

  mrc_bloatware_configfile=${mrc_device_patches_dir}/main/bloatware.cfg

  if [[ ! -f ${mrc_bloatware_configfile} ]]; then

    echo "Configuration file for bloatware removal not found"

  else

    while IFS="|" read cmd_to_execute string_0 string_1 string_2; do
      if [[ ${cmd_to_execute} == "delete" ]]; then
        ${usesudo}rm -rf ${string_0}
      fi
    done < ${mrc_bloatware_configfile}

  fi

}

run_ok "pl_delete_commonfiles" "Deleting bloatware"

pl_move_apps(){
  cd ${mrc_work_dir} || exit

  mrc_movefiles_configfile=${mrc_device_patches_dir}/main/movefiles.cfg

  if [[ ! -f ${mrc_movefiles_configfile} ]]; then

    echo "Configuration file for file move not found"

  else

    while IFS="|" read cmd_to_execute sourcefile destfile; do
      if [[ ${cmd_to_execute} == "move" ]]; then
        mv ${sourcefile} ${destfile}
      fi
    done < ${mrc_movefiles_configfile}

  fi

}

run_ok "pl_move_apps" "Moving apps"
