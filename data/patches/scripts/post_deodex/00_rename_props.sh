#!/bin/bash
# MRC - MIUI ROM Compiler
# adapted from realtebo's work on MIUI.it compiler
# Rename props in build.prop

pl_rename_props() {

  cd ${mrc_work_dir} || exit

  buildprop=${mrc_work_dir}/system/build.prop

  if [[ -f ${buildprop} ]]; then

    # Renames original build fingerprint so it cam be replaced by Google Certified ones
    echo "Renaming original build fingerprint..."
    sed -i 's/ro.build.description/ro.build.description_real/g' ${buildprop}
    sed -i 's/ro.build.fingerprint/ro.build.fingerprint_real/g' ${buildprop}

    # If custom version number is set, change it accordingly
    if [[ ${buildversion} != "Auto" ]]; then
      echo "Changing build version..."
      sed -i "/ro.build.version.incremental=/d" ${buildprop}
      sed -i "$ a ro.build.version.incremental=${romversion}" ${buildprop}
    fi

    sed -i "/ro.expect.recovery_id=/d" ${buildprop}

    # Add Google props
    echo "Adding google props..."
    if [[ ${androidinfo} == "6.0" ]]; then
      sed -i '$ a ro.com.google.gmsversion=6.0_r2' ${buildprop}

    elif [[ ${androidinfo} == "7.0" ]]; then
      sed -i '$ a ro.com.google.gmsversion=7.0_r3' ${buildprop}
      sed -i '$ a ro.setupwizard.require_network=any' ${buildprop}
      sed -i -e '$a sys.display-size=3480x2160' ${buildprop}
      sed -i '/#mm.enable.qcom_parser/d' ${buildprop}
      sed -i '/mm.enable.qcom_parser/d' ${buildprop}
      sed -i -e '$a mm.enable.qcom_parser=3183219' ${buildprop}

    elif [[ ${androidinfo} == "7.1" ]]; then
      sed -i '$ a ro.com.google.gmsversion=7.1_r7' ${buildprop}
      sed -i '$ a ro.setupwizard.require_network=any' ${buildprop}
      sed -i -e '$a sys.display-size=3480x2160' ${buildprop}
      sed -i '/#mm.enable.qcom_parser/d' ${buildprop}
      sed -i '/mm.enable.qcom_parser/d' ${buildprop}
      sed -i -e '$a mm.enable.qcom_parser=3183219' ${buildprop}

    elif [[ ${androidinfo} == "8.0" ]]; then
      sed -i '$ a ro.com.google.gmsversion=8.0_r4' ${buildprop}
      sed -i '$ a ro.setupwizard.require_network=any' ${buildprop}
      sed -i -e '$a sys.display-size=3480x2160' ${buildprop}

    elif [[ "${androidinfo}" == "8.1" ]]; then
      sed -i '$ a ro.com.google.gmsversion=8.1_r1' ${buildprop}
      sed -i '$ a ro.setupwizard.require_network=any' ${buildprop}
      sed -i -e '$a sys.display-size=3480x2160' ${buildprop}

    elif [[ "${androidinfo}" == "9.0" ]]; then
      sed -i '$ a ro.com.google.gmsversion=8.1_r1' ${buildprop}
      sed -i '$ a ro.setupwizard.require_network=any' ${buildprop}
      sed -i -e '$a sys.display-size=3480x2160' ${buildprop}
    fi

    echo "Setting compiling date..."
    epochdate="$(date +%s)"
    utcdate="$(date -u -d @${epochdate})"
    sed -i "/ro.build.date=/d" ${buildprop}
    sed -i "/ro.build.date.utc=/d" ${buildprop}
    sed -i "$ a ro.build.date=${utcdate}" ${buildprop}
    sed -i "$ a ro.build.date.utc=${epochdate}" ${buildprop}

  fi

  vendorprop=${mrc_work_dir}/vendor/build.prop

  if [[ -f ${vendorprop} ]]; then
    echo "Renaming build fingerprint from vendor..."

    sed -i 's/ro.build.description/ro.build.description_real/g' ${vendorprop}
    sed -i 's/ro.build.fingerprint/ro.build.fingerprint_real/g' ${vendorprop}
    sed -i 's/ro.vendor.build.fingerprint/ro.vendor.build.fingerprint_real/g' ${vendorprop}
    sed -i 's/ro.vendor.build.description/ro.vendor.build.description_real/g' ${vendorprop}

    if [[ ${devicename} == "dipper" ]]; then
      sed -i -e '$a ro.vendor.build.fingerprint=Xiaomi/dipper/dipper:8.1.0/OPM1.171019.011/V9.6.1.0.OEAMIFD:user/release-keys' ${vendorprop}
      sed -i -e '$a ro.vendor.build.description=dipper-user 8.1.0 OPM1.171019.011 V9.6.1.0.OEAMIFD release-keys' ${vendorprop}

    elif [[ ${devicename} == "sirius" ]]; then
      sed -i -e '$a ro.vendor.build.fingerprint=Xiaomi/sirius/sirius:8.1.0/OPM1.171019.019/V9.5.8.0.OEBCNFA:user/release-keys' ${vendorprop}
      sed -i -e '$a ro.vendor.build.description=sirius-user 8.1.0 OPM1.171019.019 V9.5.8.0.OEBCNFA release-keys' ${vendorprop}

    elif [[ ${devicename} == "polaris" ]]; then
      sed -i -e '$a ro.vendor.build.fingerprint=Xiaomi/polaris/polaris:8.0.0/OPR1.170623.032/V9.5.4.0.ODGMIFA:user/release-keys' ${vendorprop}
      sed -i -e '$a ro.vendor.build.description=polaris-user 8.0.0 OPR1.170623.032 V9.5.4.0.ODGMIFA release-keys' ${vendorprop}

    elif [[ ${devicename} == "cactus" ]]; then
      sed -i -e '$a ro.vendor.build.fingerprint=xiaomi/cactus/cactus:8.1.0/O11019/V9.6.2.0.OCBMIFD:user/release-keys' ${vendorprop}
      sed -i -e '$a ro.vendor.build.description=cactus-user 8.1.0 O11019 V9.6.2.0.OCBMIFD release-keys' ${vendorprop}

    elif [[ ${devicename} == "cereus" ]]; then
      sed -i -e '$a ro.vendor.build.fingerprint=xiaomi/cereus/cereus:8.1.0/O11019/V9.6.14.0.OCGMIFD:user/release-keys' ${vendorprop}
      sed -i -e '$a ro.vendor.build.description=cereus-user 8.1.0 O11019 V9.6.14.0.OCGMIFD release-keys' ${vendorprop}

    elif [[ ${devicename} == "ysl" ]]; then
      sed -i -e '$a ro.vendor.build.fingerprint=xiaomi/ysl/ysl:8.1.0/OPM1.171019.011/V9.5.14.0.OEFMIFA:user/release-keys' ${vendorprop}
      sed -i -e '$a ro.vendor.build.description=ysl-user 8.1.0 OPM1.171019.011 V9.5.14.0.OEFMIFA release-keys' ${vendorprop}

    elif [[ ${devicename} == "whyred" ]]; then
      sed -i -e '$a ro.vendor.build.fingerprint=xiaomi/whyred/whyred:7.1.1/NGI77B/V9.2.6.0.NEIMIEK:user/release-keys' ${vendorprop}
      sed -i -e '$a ro.vendor.build.description=whyred-user 7.1.1 NGI77B V9.2.6.0.NEIMIEK release-keys' ${vendorprop}

    else
      sed -i -e '$a ro.vendor.build.fingerprint=Xiaomi/sagit/sagit:7.1.1/NMF26X/V8.2.17.0.NCACNEC:user/release-keys' ${vendorprop}
      sed -i -e '$a ro.vendor.build.description=sagit-user 7.1.1 NMF26X V8.2.17.0.NCACNEC release-keys' ${vendorprop}
    fi

  fi

  propdefault=${mrc_work_dir}/system/etc/prop.default

  if [[ -f ${propdefault} ]]; then
    echo "Removing bootimage fingerprint from default prop..."
    sed -i "/ro.bootimage.build.fingerprint=/d" ${propdefault}
  fi

}

run_ok "pl_rename_props" "Making changes on build.prop"
