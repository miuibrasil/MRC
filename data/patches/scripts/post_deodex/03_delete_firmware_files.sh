#!/bin/bash
# MRC - MIUI ROM Compiler
# adapted from realtebo's work on MIUI.it compiler
# Delete unused firmware files

pl_delete_firmware(){
  cd ${mrc_work_dir} || exit

  mrc_firmware_configfile=${mrc_device_patches_dir}/devices/${devicename}/firmware.cfg

  if [[ ! -f ${mrc_firmware_configfile} ]]; then

    echo "Configuration file for ${devicename} not found"

  else

    while IFS="|" read cmd_to_execute string_0 string_1 string_2; do
      if [[ ${cmd_to_execute} == "delete" ]]; then
        ${usesudo}rm -rf firmware-update/${string_0}
      fi
    done < ${mrc_firmware_configfile}

  fi

}

run_ok "pl_delete_firmware" "Deleting unused firmware-update files"
