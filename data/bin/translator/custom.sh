#!/bin/bash

echo "Fixing Settings.apk..."
cd ${mrc_bin_dir}/translator/projects/input.zip.bzprj/baseROM/system/priv-app/Settings || exit
unzip -q Settings.apk -d unzipped
cd unzipped
zip -0qr Settings.zip *
rm -rf ../Settings.apk
mv Settings.zip ../Settings.apk
cd ..
rm -rf unzipped

echo "Setting up ROM version..."
cd ${mrc_bin_dir}/translator/projects/input.zip.bzprj/baseROM/system || exit
sed -i "/ro.build.version.incremental/d" build.prop
sed -i "$ a ro.build.version.incremental=${romversion}" build.prop
