#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Binaries used in the system

# Compression & Optimization
export brotli="${mrc_bin_dir}/compression/brotli"
export p7z="${mrc_bin_dir}/compression/7z"
export zipalign="${mrc_bin_dir}/misc/zipalign"

# APK handling
export aapt="${mrc_bin_dir}/apktool/aapt"
export apktool="${mrc_bin_dir}/apktool/apktool"
export vdexext="${mrc_bin_dir}/misc/vdexExtractor"
export vdexext2="${mrc_bin_dir}/misc/vdexExtractor2"
export cdexext="${mrc_bin_dir}/misc/compact_dex_converter"
export zipsigner="java -jar ${mrc_bin_dir}/misc/signapk.jar ${mrc_config_dir}/certs/certificate.x509.pem ${mrc_config_dir}/certs/certificate.pk8"

# IMG/Filesystem tools
export make_ext4fs="${mrc_bin_dir}/ext4_tools/make_ext4fs"
export ext2simg="${mrc_bin_dir}/ext4_tools/ext2simg"
export simg2img="${mrc_bin_dir}/ext4_tools/simg2img"
export img2sdat="python ${mrc_bin_dir}/img2sdat/img2sdat.py"
export sdat2img="python ${mrc_bin_dir}/sdat2img/sdat2img.py"
export getmetadata="python3.6 ${mrc_bin_dir}/misc/getmeta.py"

# Kernel tools
export verityoff="python3.6 ${mrc_bin_dir}/misc/rmverity.py"
export selpatch="${mrc_bin_dir}/misc/bootimg"

# Translation tools
export crowdin="java -jar ${mrc_bin_dir}/misc/crowdin-cli.jar"

# Translator
export translator="xvfb-run java -jar ${mrc_bin_dir}/translator/jbart3h.jar"

# Marshmallow deodexer
export deomm="xvfb-run java -jar ${mrc_bin_dir}/LODTRTA/Launcher.jar"

# Smali patcher
export smalip="${mrc_bin_dir}/patcher/smali_patch"
