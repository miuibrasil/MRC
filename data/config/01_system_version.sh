#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Systemwide variables

# System version
export mrc_version="0.3 (Beta)"
export buildversion="Auto"
export maindevice="sagit"
export projectname="MIUIAM"

# Compiler repositories
export mrc_commonxml_repo="git@gitlab.com:MRCDev/MRC-Common-XML.git"
export mrc_commonxml_repo_dir="${mrc_repositories_dir}/MRC-Common-XML"

export mrc_extras_repo="git@gitlab.com:MRCDev/MRC-extras.git"
export mrc_extras_repo_dir="${mrc_repositories_dir}/MRC-extras"

export mrc_extraschina_repo="git@gitlab.com:MRCDev/MRC-extras-China.git"
export mrc_extraschina_repo_dir="${mrc_repositories_dir}/MRC-extras-China"

export mrc_gapps_repo="git@gitlab.com:MRCDev/MRC-gapps.git"
export mrc_gapps_repo_dir="${mrc_repositories_dir}/MRC-gapps"

export mrc_globalxml_repo="git@gitlab.com:MRCDev/MRC-Global-XML.git"
export mrc_globalxml_repo_dir="${mrc_repositories_dir}/MRC-Global-XML"

export mrc_sourcexml_repo="git@gitlab.com:MRCDev/MRC-Source-XML.git"
export mrc_sourcexml_repo_dir="${mrc_repositories_dir}/MRC-Source-XML"

export mrc_xmlpatches_repo="git@gitlab.com:MRCDev/MRC-Patches-XML.git"
export mrc_xmlpatches_repo_dir="${mrc_repositories_dir}/MRC-Patches-XML"
