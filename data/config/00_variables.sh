#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Directories config file

# Data dir: contains all files used by our compiler
# MRC/data
export mrc_data_dir=${mrc_system}/data

# Bin subdir: contains all binaries used by our compiler scripts
# MRC/data/bin
export mrc_bin_dir=${mrc_data_dir}/bin

# Config subdir: contains all configuration files used by our compiler
# MRC/data/config
export mrc_config_dir=${mrc_data_dir}/config

# Device patches subdir: contains all patches used by our compiler
# MRC/data/device_patches
export mrc_device_patches_dir=${mrc_data_dir}/patches

# Logs subdir: contains all logs generated by our compiler
# MRC/data/logs
export mrc_log_dir=${mrc_data_dir}/logs

# Scripts subdir: contains all scripts used by our compiler
# MRC/data/scripts
export mrc_scripts_dir=${mrc_data_dir}/scripts

# core scripts subdir: contains all core scripts used by our compiler
# MRC/data/scripts/core
export mrc_scripts_core_dir=${mrc_scripts_dir}/core

# Deodex scripts subdir: contains all scripts related to our deodex system
# MRC/data/scripts/deodex
export mrc_scripts_deodex_dir=${mrc_scripts_dir}/deodex

# Menu scripts subdir: contains all menufiles for items in our compiler
# MRC/data/scripts/menus
export mrc_scripts_menu_dir=${mrc_scripts_dir}/menus

# ROM scripts subdir: contains all scripts related to ROM work used by our compiler
# MRC/data/scripts/rom
export mrc_scripts_rom_dir=${mrc_scripts_dir}/rom

# Repositories dir: contains all repositories used during compile phase
# MRC/data/repositories
export mrc_repositories_dir=${mrc_data_dir}/repositories

# Workdir: temporary folder where deodexing / patches takes place
# MRC/data/workdir
export mrc_work_dir=${mrc_data_dir}/workdir

# updater-script subdir: Placeholder for folder creation when converting our ROMs
# MRC/data/workdir/META-INF/com/google/android
export mrc_work_dir_updater=${mrc_work_dir}/META-INF/com/google/android

# Project files subdir: Placeholder for all files related to our ROM conversion method.
# MRC/data/workdir/project_files
export mrc_work_dir_project=${mrc_work_dir}/project_files

# Input directory where untouched ROMs are located
# MRC/input/source
export mrc_source_rom=${mrc_system}/input/source

# Output directory where processed ROMs are moved to
# MRC/input/source/done
export mrc_source_done=${mrc_source_rom}/done

# Input directory where deodexed ROMs are located after conversion
# MRC/input/deodexed
export mrc_deodexed_rom=${mrc_system}/input/deodexed

# Output directory where processed deodexed ROMs are moved to
# MRC/input/deodexed/done
export mrc_deodexed_done=${mrc_deodexed_rom}/done

export mrc_translator_dir=${mrc_bin_dir}/translator

export mrc_translator_input_dir=${mrc_translator_dir}/input

export mrc_translator_output_dir=${mrc_translator_dir}/output

# Output directory where compiled ROMs are placed
# MRC/output
export mrc_output_dir=${mrc_system}/output

# Smali Patches dir
export mrc_smali_patches_dir="${mrc_repositories_dir}/MRC-Smali-Patches"
