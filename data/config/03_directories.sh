#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira
# Create directories if they don't exist

# Data directory
if [[ ! -d ${mrc_data_dir} ]]; then
  mkdir -p ${mrc_data_dir}
fi

# Bin directory
if [[ ! -d ${mrc_bin_dir} ]]; then
  mkdir -p ${mrc_bin_dir}
fi

# Config directory
if [[ ! -d ${mrc_config_dir} ]]; then
  mkdir -p ${mrc_config_dir}
fi

# Logs directory
if [[ ! -d ${mrc_log_dir} ]]; then
  mkdir -p ${mrc_log_dir}
fi

# Working directory
if [[ ! -d ${mrc_work_dir} ]]; then
  mkdir -p ${mrc_work_dir}
fi

# ROM Input Directories
if [[ ! -d ${mrc_source_done} ]]; then
  mkdir -p ${mrc_source_done}
fi
if [[ ! -d ${mrc_translator_input_dir} ]]; then
  mkdir -p ${mrc_translator_input_dir}
fi

# ROM Output Directories
if [[ ! -d ${mrc_deodexed_done} ]]; then
  mkdir -p ${mrc_deodexed_done}
fi
if [[ ! -d ${mrc_translator_output_dir} ]]; then
  mkdir -p ${mrc_translator_output_dir}
fi
if [[ ! -d ${mrc_output_dir} ]]; then
  mkdir -p ${mrc_output_dir}
fi

# Device patches
if [[ ! -d ${mrc_device_patches_dir}/scripts/post_build ]]; then
  mkdir -p ${mrc_device_patches_dir}/scripts/post_build
fi
if [[ ! -d ${mrc_device_patches_dir}/scripts/post_deodex ]]; then
  mkdir -p ${mrc_device_patches_dir}/scripts/post_deodex
fi
if [[ ! -d ${mrc_device_patches_dir}/scripts/pre_build ]]; then
  mkdir -p ${mrc_device_patches_dir}/scripts/pre_build
fi
if [[ ! -d ${mrc_device_patches_dir}/scripts/pre_deodex ]]; then
  mkdir -p ${mrc_device_patches_dir}/scripts/pre_deodex
fi

# Repositories
if [[ ! -d ${mrc_commonxml_repo_dir} ]]; then
  git clone ${mrc_commonxml_repo} ${mrc_commonxml_repo_dir}
fi
if [[ ! -d ${mrc_extras_repo_dir} ]]; then
  git clone ${mrc_extras_repo} ${mrc_extras_repo_dir}
fi
if [[ ! -d ${mrc_extraschina_repo_dir} ]]; then
  git clone ${mrc_extraschina_repo} ${mrc_extraschina_repo_dir}
fi
if [[ ! -d ${mrc_gapps_repo_dir} ]]; then
  git clone ${mrc_gapps_repo} ${mrc_gapps_repo_dir}
fi
if [[ ! -d ${mrc_globalxml_repo_dir} ]]; then
  git clone ${mrc_globalxml_repo} ${mrc_globalxml_repo_dir}
fi
if [[ ! -d ${mrc_xmlpatches_repo_dir} ]]; then
  git clone ${mrc_xmlpatches_repo} ${mrc_xmlpatches_repo_dir}
fi
if [[ ! -d ${mrc_sourcexml_repo_dir} ]]; then
  mkdir -p ${mrc_sourcexml_repo_dir}
fi
