#!/bin/bash
# MRC - MIUI ROM Compiler
# by Matheus Ferreira

# If there isn't a .mrc file on the config directory, creates a file containing the compiler directory.
if [[ ! -f data/config/.mrc ]]; then
  echo ${PWD} >> data/config/.mrc
# If .mrc file differs in content than the current compiler directory, deletes it and creates with the correct value.
elif [[ $(echo ${PWD}) != $(cat data/config/.mrc) ]]; then
  rm -rf data/config/.mrc
    echo ${PWD} >> data/config/.mrc
fi

# Exports the main variable
export mrc_system="$(cat data/config/.mrc)"

# Load system config
for mrc_sys_config in ${mrc_system}/data/config/*.sh; do
  . ${mrc_sys_config}
done

# If there's core functions located in the core folder, loads them.
if [[ $(ls ${mrc_scripts_core_dir}) != "" ]]; then
  for mrc_scripts_core in ${mrc_scripts_core_dir}/*.sh; do
    . ${mrc_scripts_core}
  done
fi

# If there's deodex functions located in the deodex folder, loads them.
if [[ $(ls ${mrc_scripts_deodex_dir}) != "" ]]; then
  for mrc_scripts_deodex in ${mrc_scripts_deodex_dir}/*.sh; do
    . ${mrc_scripts_deodex}
  done
fi

# If there's rom functions located in the rom folder, loads them.
if [[ $(ls ${mrc_scripts_rom_dir}) != "" ]]; then
  for mrc_scripts_rom in ${mrc_scripts_rom_dir}/*.sh; do
    . ${mrc_scripts_rom}
  done
fi

# Load menu files to construct our main menu
if [[ $(ls ${mrc_scripts_menu_dir}) != "" ]]; then
  for mrc_scripts_menu in ${mrc_scripts_menu_dir}/*.sh; do
    . ${mrc_scripts_menu}
  done
fi

# Cleans compiler temporary directories
cleanup
clean_logs

# Load main menu
mainmenu
